const {SidebarFragment}= require('../support/page_objects/sidebar/fragments')
const {LoginConstants} = require("../constants");
const {LoginPage} = require("../support/page_objects/login/fragments");


describe('Sidebar', () => {

    before(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Sidebar elements check', () => {

        const sidebar = new SidebarFragment()

        sidebar.getMainSidebar().screenshot({capture: "fullPage"})

        //Wait for elements of sidebar to be visible
        sidebar.getMainSidebar().should('be.visible')
        sidebar.getBrandLink().should('be.visible').and('contain.text', 'Proper Eating')
        sidebar.getUserPanel().should('be.visible')
        sidebar.getDishesDropdown().should('be.visible')
        sidebar.getCreateDishTab().should('be.visible')
        sidebar.getViewDishesTab().should('be.visible')
        sidebar.getCategoriesDropdown().should('be.visible')
        sidebar.getViewCategoriesTab().should('be.visible')
        sidebar.getCreateSubcategoryTab().should('be.visible')
        sidebar.getCreateCategoryTab().should('be.visible')
        sidebar.getDrinkCategoriesDropdown().should('be.visible')
        sidebar.getCreateDrinkTab().should('be.visible')
        sidebar.getViewDrinkTab().should('be.visible')
        sidebar.getLogoutButton().should('be.visible')

    })

    it('Dropdowns work check', () => {

        const sidebar = new SidebarFragment()


        //Check dishes dropdown work
        sidebar.getDishesDropdown().click()
        sidebar.getMainSidebar().screenshot({capture: "fullPage"})
        sidebar.getCreateDishTab().should('not.be.visible')
        sidebar.getViewDishesTab().should('not.be.visible')
        sidebar.getCategoriesDropdown().should('be.visible')

        sidebar.getViewCategoriesTab().should('be.visible')
        sidebar.getCreateSubcategoryTab().should('be.visible')
        sidebar.getCreateCategoryTab().should('be.visible')
        sidebar.getDrinkCategoriesDropdown().should('be.visible')
        sidebar.getCreateDrinkTab().should('be.visible')
        sidebar.getViewDrinkTab().should('be.visible')

        sidebar.getDishesDropdown().click()
        sidebar.getCreateDishTab().should('be.visible')
        sidebar.getViewDishesTab().should('be.visible')

        //Check categories dropdown
        sidebar.getCategoriesDropdown().click()
        sidebar.getMainSidebar().screenshot({capture: "fullPage"})
        sidebar.getViewCategoriesTab().should('not.be.visible')
        sidebar.getCreateSubcategoryTab().should('not.be.visible')
        sidebar.getCreateCategoryTab().should('not.be.visible')

        sidebar.getDishesDropdown().should('be.visible')
        sidebar.getCreateDishTab().should('be.visible')
        sidebar.getViewDishesTab().should('be.visible')
        sidebar.getDrinkCategoriesDropdown().should('be.visible')
        sidebar.getCreateDrinkTab().should('be.visible')
        sidebar.getViewDrinkTab().should('be.visible')

        sidebar.getCategoriesDropdown().click()
        sidebar.getViewCategoriesTab().should('be.visible')
        sidebar.getCreateSubcategoryTab().should('be.visible')
        sidebar.getCreateCategoryTab().should('be.visible')

        //Check drinks categories dropdown
        sidebar.getDrinkCategoriesDropdown().click()
        sidebar.getMainSidebar().screenshot({capture: "fullPage"})
        sidebar.getMainSidebar().screenshot({capture: "fullPage"})
        sidebar.getCreateDrinkTab().should('not.be.visible')
        sidebar.getViewDrinkTab().should('not.be.visible')

        sidebar.getDishesDropdown().should('be.visible')
        sidebar.getCreateDishTab().should('be.visible')
        sidebar.getViewDishesTab().should('be.visible')
        sidebar.getCategoriesDropdown().should('be.visible')
        sidebar.getViewCategoriesTab().should('be.visible')
        sidebar.getCreateSubcategoryTab().should('be.visible')
        sidebar.getCreateCategoryTab().should('be.visible')

        sidebar.getDrinkCategoriesDropdown().click()
        sidebar.getCreateDrinkTab().should('be.visible')
        sidebar.getViewDrinkTab().should('be.visible')
    })

    it('Should log out user and redirect to the login page', () => {

        const sidebar = new SidebarFragment()
        const loginPage = new LoginPage()

        sidebar.getLogoutButton().click()

        loginPage.getLoginBox().should('be.visible')
        loginPage.getEmailInput().should('be.visible')
        loginPage.getPasswordInput().should('be.visible')
        loginPage.getRememberMeLabel().should('be.visible')
        loginPage.getSignInButton().should('be.visible')
        loginPage.getForgotPasswordLink().should('be.visible')
        loginPage.getRegisterLink().should('be.visible')
    })

})