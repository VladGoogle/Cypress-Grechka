const {CreateDishPage} = require('../support/page_objects/create_dish/fragments')
const {CreateDishErrors} = require('../support/page_objects/create_dish/errors')
const {CreateDishConstants, LoginConstants} = require("../constants");
const {SidebarFragment} = require("../support/page_objects/sidebar/fragments");
const {ViewDishesPage} = require("../support/page_objects/view_dishes/fragments");
const {ViewDishesErrors} = require("../support/page_objects/view_dishes/errors");


describe('Create dish', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Modal check', () => {

        const createDishFragment = new CreateDishPage()
        const sidebar = new SidebarFragment()

        sidebar.getCreateDishTab().click()

        createDishFragment.getCreateDishTitle().should('be.visible')
        createDishFragment.getNameInput().should('be.visible')
        createDishFragment.getShortNameInput().should('be.visible')
        createDishFragment.getDescriptionInput().should('be.visible')
        createDishFragment.getProteinsInput().should('be.visible')
        createDishFragment.getFatsInput().should('be.visible')
        createDishFragment.getCarbsInput().should('be.visible')
        createDishFragment.getFibersInput().should('be.visible')
        createDishFragment.getCaloriesInput().should('be.visible')
        createDishFragment.getSpoonGramsInput().should('be.visible')
        createDishFragment.getPalmGramsInput().should('be.visible')
        createDishFragment.getCategorySelector().should('be.visible')
        createDishFragment.getCategorySelector().should('be.visible')
        createDishFragment.getSubcategorySelector().should('be.visible')
        createDishFragment.getVitaminsSelector().should('be.visible')
        createDishFragment.getSubmitButton().should('be.visible')
        createDishFragment.getViewDishLink().should('be.visible')
    })

    it('Required errors check', () => {

        const createDishFragment = new CreateDishPage()
        const createDishErrors = new CreateDishErrors()
        const sidebar = new SidebarFragment()

        sidebar.getCreateDishTab().click()
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})

        createDishErrors.getNameRequiredError().should('be.visible')
        createDishErrors.getDescriptionRequiredError().should('be.visible')
        createDishErrors.getPlateImageRequiredError().should('be.visible')
        createDishErrors.getSearchImageRequiredError().should('be.visible')
        createDishErrors.getProteinsRequiredError().should('be.visible')
        createDishErrors.getFatsRequiredError().should('be.visible')
        createDishErrors.getCarbsRequiredError().should('be.visible')
        createDishErrors.getFibersRequiredError().should('be.visible')
        createDishErrors.getCaloriesRequiredError().should('be.visible')
        createDishErrors.getSpoonGramsRequiredError().should('be.visible')
        createDishErrors.getPalmGramsRequiredError().should('be.visible')
        createDishErrors.getCategoryRequiredError().should('be.visible')
        createDishErrors.getSubcategoryRequiredError().should('be.visible')
        createDishErrors.getVitaminsRequiredError().should('be.visible')

    })

    it('Validation errors check', () => {

        const dishConstant = new CreateDishConstants()
        const createDishFragment = new CreateDishPage()
        const createDishErrors = new CreateDishErrors()
        const sidebar = new SidebarFragment()

        sidebar.getCreateDishTab().click()

        //Name too long string check
        createDishFragment.getNameInput().type(dishConstant.randomNameLargestring)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getNameInvalidError().should('be.visible')

        //Short name without second word
        createDishFragment.getShortNameInput().type(dishConstant.randomShortNameSecondNullString)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getShortNameInvalidError().should('be.visible')

        //Short name invalid max length
        createDishFragment.getShortNameInput().clear().type(dishConstant.randomShortNameMaxString)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getShortNameMaxError().should('be.visible')

        //Short name starts with a lower case letter
        createDishFragment.getShortNameInput().clear().type(dishConstant.randomShortNameLowerString)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getShortNameInvalidError().should('be.visible')

        //Description too long string
        createDishFragment.getDescriptionInput().type(dishConstant.randomDescriptionLargeString)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getDescriptionMaxError().should('be.visible')

        //Upload images with valid format and invalid size
        cy.uploadDishImage(dishConstant.validFormatInvalidSizeFilePath)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getPlateImageInvalidSizeError().should('be.visible')
        createDishErrors.getSearchImageInvalidSizeError().should('be.visible')
        createDishErrors.getFullImageInvalidSizeError().should('be.visible')
        createDishErrors.getHalfImageInvalidSizeError().should('be.visible')
        createDishErrors.getThirdImageInvalidSizeError().should('be.visible')
        createDishErrors.getQuarterImageInvalidSizeError().should('be.visible')

        //Upload image with invalid format and valid size
        cy.uploadDishImage(dishConstant.invalidFormatValidSizeFilePath)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getPlateImageInvalidFormatError().should('be.visible')
        createDishErrors.getSearchImageInvalidFormatError().should('be.visible')
        createDishErrors.getFullImageInvalidFormatError().should('be.visible')
        createDishErrors.getHalfImageInvalidFormatError().should('be.visible')
        createDishErrors.getThirdImageInvalidFormatError().should('be.visible')
        createDishErrors.getQuarterImageInvalidFormatError().should('be.visible')

        //Upload image with invalid format and invalid size
        cy.uploadDishImage(dishConstant.invalidFormatInvalidSizeFilePath)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})
        createDishErrors.getPlateImageInvalidFormatError().should('be.visible')
        createDishErrors.getSearchImageInvalidFormatError().should('be.visible')
        createDishErrors.getFullImageInvalidFormatError().should('be.visible')
        createDishErrors.getHalfImageInvalidFormatError().should('be.visible')
        createDishErrors.getThirdImageInvalidFormatError().should('be.visible')
        createDishErrors.getQuarterImageInvalidFormatError().should('be.visible')


        //Check drag-n-drop function
        createDishFragment.getPlateImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getSearchImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getFullImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getHalfImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getThirdImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getQuarterImageInput().selectFile(dishConstant.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDishFragment.getSubmitButton().click()

        //Check Browser validation messages

        //Check Browser min validation msg
        cy.validateMinBrowserErrorMsg('proteinsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('fatsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('carbsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('fibersPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('caloriesPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('gramsInSpoon', 'Submit')
        cy.validateMinBrowserErrorMsg('gramsInPalm', 'Submit')

        //Check Browser float validation msg
        cy.validateFloatBrowserErrorMsg('proteinsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('fatsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('carbsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('fibersPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('caloriesPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('gramsInSpoon', 'Submit')
        cy.validateFloatBrowserErrorMsg('gramsInPalm', 'Submit')
    })

    it('Should create, update and delete dish',  () =>{

        const createDishConstant = new CreateDishConstants()
        const createDishFragment = new CreateDishPage()
        const viewDishFragment = new ViewDishesPage()
        const createDishErrors = new CreateDishErrors()
        const sidebar = new SidebarFragment()
        const viewDishesErrors = new ViewDishesErrors()

        sidebar.getCreateDishTab().click()

        //Create dish
        createDishFragment.getNameInput().type(createDishConstant.randomName)
        createDishFragment.getShortNameInput().type(createDishConstant.randomShortName)
        createDishFragment.getDescriptionInput().type(createDishConstant.randomDescription)
        cy.uploadDishImage(createDishConstant.validFormatValidSizeFilePath)
        cy.enterNutrients('proteinsPer100Grams')
        cy.enterNutrients('fatsPer100Grams')
        cy.enterNutrients('carbsPer100Grams')
        cy.enterNutrients('fibersPer100Grams')
        cy.enterNutrients('caloriesPer100Grams')
        cy.enterNutrients('gramsInSpoon')
        cy.enterNutrients('gramsInPalm')
        createDishFragment.getCategorySelector().select(3)
        createDishFragment.getSubcategorySelector().select(5)
        createDishFragment.getVitaminsSelector().select(4)
        createDishFragment.getSubmitButton().click()
        createDishFragment.getContentWrapper().screenshot({capture: "fullPage"})
        createDishErrors.getSuccessCreateDishMsg().should('be.visible')

        //Go to View Dishes page
        sidebar.getViewDishesTab().click()
        viewDishFragment.getNameInput().type(createDishConstant.randomName)
        viewDishFragment.getSearchButton().click()
        viewDishFragment.getTableBlock().find('td:nth-child(3)').should('be.visible').and('have.text', createDishConstant.randomName)

        //Click on edit button
        cy.getEditButton(createDishConstant.randomName)
        createDishFragment.getNameInput().should('have.value', createDishConstant.randomName)
        createDishFragment.getShortNameInput().should('have.value', createDishConstant.randomShortName)
        createDishFragment.getDescriptionInput().should('have.value', createDishConstant.randomDescription)

        //Update dish values
        createDishFragment.getNameInput().clear().type(createDishConstant.updateRandomName)
        createDishFragment.getShortNameInput().clear().type(createDishConstant.updateRandomShortName)
        createDishFragment.getDescriptionInput().clear().type(createDishConstant.updateRandomDescription)
        cy.uploadDishImage(createDishConstant.secondValidFormatValidSizeFilePath)
        cy.enterNutrients('proteinsPer100Grams')
        cy.enterNutrients('fatsPer100Grams')
        cy.enterNutrients('carbsPer100Grams')
        cy.enterNutrients('fibersPer100Grams')
        cy.enterNutrients('caloriesPer100Grams')
        cy.enterNutrients('gramsInSpoon')
        cy.enterNutrients('gramsInPalm')
        createDishFragment.getCategorySelector().select(5)
        createDishFragment.getSubcategorySelector().select(1)
        createDishFragment.getVitaminsSelector().select(2)
        createDishFragment.getSubmitButton().click()
        createDishErrors.getSuccessUpdateDishMsg().should('be.visible')

        //Go to View Dishes page
        sidebar.getViewDishesTab().click()
        viewDishFragment.getNameInput().type(createDishConstant.updateRandomName)
        viewDishFragment.getSearchButton().click()
        viewDishFragment.getTableBlock().get('td:nth-child(3)').should('be.visible').and('have.text', createDishConstant.updateRandomName)
        viewDishFragment.getTableBlock().get('td:nth-child(4)').should('be.visible').and('have.text', createDishConstant.updateRandomDescription)

        //Delete dish
        cy.getDeleteButton(createDishConstant.updateRandomName)
        viewDishFragment.getNameInput().type(createDishConstant.updateRandomName)
        viewDishFragment.getSearchButton().click()
        viewDishesErrors.getNoRecordsFoundError().should('be.visible')
    });


    it('Should fix FIT-386',  () =>{

        const createDishConstant = new CreateDishConstants()
        const createDishFragment = new CreateDishPage()
        const viewDishFragment = new ViewDishesPage()
        const createDishErrors = new CreateDishErrors()
        const sidebar = new SidebarFragment()

        sidebar.getCreateDishTab().click()

        //Create dish
        createDishFragment.getNameInput().type(createDishConstant.randomName)
        createDishFragment.getShortNameInput().type(createDishConstant.randomShortName)
        createDishFragment.getDescriptionInput().type(createDishConstant.randomDescription)
        cy.uploadDishImage(createDishConstant.validFormatValidSizeFilePath)
        cy.enterNutrients('proteinsPer100Grams')
        cy.enterNutrients('fatsPer100Grams')
        cy.enterNutrients('carbsPer100Grams')
        cy.enterNutrients('fibersPer100Grams')
        cy.enterNutrients('caloriesPer100Grams')
        cy.enterNutrients('gramsInSpoon')
        cy.enterNutrients('gramsInPalm')
        createDishFragment.getCategorySelector().select(3)
        createDishFragment.getSubcategorySelector().select(5)
        createDishFragment.getVitaminsSelector().select(4)
        createDishFragment.getSubmitButton().click()
        createDishErrors.getSuccessCreateDishMsg().should('be.visible')
        createDishFragment.getContentWrapper().screenshot({capture: "fullPage"})


        //Go to View Dishes page
        sidebar.getViewDishesTab().click()
        viewDishFragment.getNameInput().type(createDishConstant.randomName)
        viewDishFragment.getSearchButton().click()


        //Update dish
        cy.getEditButton(createDishConstant.randomName)

        //Edit dish name
        createDishFragment.getNameInput().clear().type(createDishConstant.updateRandomName)
        createDishFragment.getCategorySelector().select(5)
        createDishFragment.getSubcategorySelector().select(1)
        createDishFragment.getVitaminsSelector().select(2)
        createDishFragment.getSubmitButton().click()
        createDishErrors.getSuccessUpdateDishMsg().should('be.visible')
    });


})