const {CreateDrinkConstants, LoginConstants} = require("../constants");
const {SidebarFragment} = require("../support/page_objects/sidebar/fragments");
const {CreateDrinkPage} = require("../support/page_objects/create_drink/fragments");
const {CreateDrinkErrors} = require("../support/page_objects/create_drink/errors");
const {ViewDrinksPage} = require("../support/page_objects/view_drinks/fragments");
const {ViewDrinksErrors} = require("../support/page_objects/view_drinks/errors");
const {ViewDishesPage} = require("../support/page_objects/view_dishes/fragments");
const {ViewDishesErrors} = require("../support/page_objects/view_dishes/errors");


describe('Create drink', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Modal check', () => {

        const sidebar = new SidebarFragment()
        const createDrinkFragment = new CreateDrinkPage()

        sidebar.getCreateDrinkTab().click()


        createDrinkFragment.getNameInput().should('be.visible')
        createDrinkFragment.getDescriptionInput().should('be.visible')
        // createDrinkFragment.getCupImageInput().should('be.visible')
        // createDrinkFragment.getSearchImageInput().should('be.visible')
        createDrinkFragment.getProteinsInput().should('be.visible')
        createDrinkFragment.getFatsInput().should('be.visible')
        createDrinkFragment.getCarbsInput().should('be.visible')
        createDrinkFragment.getFibersInput().should('be.visible')
        createDrinkFragment.getCaloriesInput().should('be.visible')
        createDrinkFragment.getSubcategorySelector().should('be.visible')
        createDrinkFragment.getVitaminsSelector().should('be.visible')
        createDrinkFragment.getViewDishLink().should('be.visible')
    })

    it('Required errors check', () => {

        const sidebar = new SidebarFragment()
        const createDrinkFragment = new CreateDrinkPage()
        const createDrinkErrors = new CreateDrinkErrors()

        sidebar.getCreateDrinkTab().click()
        createDrinkFragment.getSubmitButton().click()

        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkErrors.getNameRequiredError().should('be.visible')
        createDrinkErrors.getDescriptionRequiredError().should('be.visible')
        createDrinkErrors.getCupImageRequiredError().should('be.visible')
        createDrinkErrors.getSearchImageRequiredError().should('be.visible')
        createDrinkErrors.getProteinsRequiredError().should('be.visible')
        createDrinkErrors.getFatsRequiredError().should('be.visible')
        createDrinkErrors.getCarbsRequiredError().should('be.visible')
        createDrinkErrors.getFibersRequiredError().should('be.visible')
        createDrinkErrors.getCaloriesRequiredError().should('be.visible')
        createDrinkErrors.getSubcategoryRequiredError().should('be.visible')
        createDrinkErrors.getVitaminsRequiredError().should('be.visible')
    })

    it('Validation errors check', () => {

        const createDrinkConstants = new CreateDrinkConstants()
        const createDrinkFragment = new CreateDrinkPage()
        const createDrinkErrors = new CreateDrinkErrors()
        const sidebar = new SidebarFragment()

        sidebar.getCreateDrinkTab().click()

        //Name too long string check
        createDrinkFragment.getNameInput().type(createDrinkConstants.randomNameLargeString)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkErrors.getNameInvalidError().should('be.visible')

        //Description too long string
        createDrinkFragment.getDescriptionInput().type(createDrinkConstants.randomDescriptionLargeString)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkErrors.getDescriptionMaxError().should('be.visible')

        //Upload image with valid format and invalid size
        cy.uploadDrinkImage(createDrinkConstants.validFormatInvalidSizeFilePath)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        // createDrinkErrors.getCupImageInvalidSizeError().should('be.visible')
        createDrinkErrors.getSearchImageInvalidSizeError().should('be.visible')

        //Upload image with invalid format and valid size
        cy.uploadDrinkImage(createDrinkConstants.invalidFormatValidSizeFilePath)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkErrors.getCupImageInvalidFormatError().should('be.visible')
        createDrinkErrors.getSearchImageInvalidFormatError().should('be.visible')

        //Upload image with invalid format and invalid size
        cy.uploadDrinkImage(createDrinkConstants.invalidFormatInvalidSizeFilePath)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkErrors.getCupImageInvalidFormatError().should('be.visible')
        createDrinkErrors.getSearchImageInvalidFormatError().should('be.visible')

        //Check drag-n-drop function
        createDrinkFragment.getCupImageInput().selectFile(createDrinkConstants.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDrinkFragment.getSearchImageInput().selectFile(createDrinkConstants.validFormatValidSizeFilePath, { action: 'drag-drop' })
        createDrinkFragment.getSubmitButton().click().wait(1000)

        //Check Browser validation messages

        //Check Browser min validation msg
        cy.validateMinBrowserErrorMsg('proteinsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('fatsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('carbsPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('fibersPer100Grams', 'Submit')
        cy.validateMinBrowserErrorMsg('caloriesPer100Grams', 'Submit')


        //Check Browser float validation msg
        cy.validateFloatBrowserErrorMsg('proteinsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('fatsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('carbsPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('fibersPer100Grams', 'Submit')
        cy.validateFloatBrowserErrorMsg('caloriesPer100Grams', 'Submit')

    })

    it('Should create,update and delete drink',  () =>{

        const createDrinkConstants = new CreateDrinkConstants()
        const createDrinkFragment = new CreateDrinkPage()
        const createDrinkErrors = new CreateDrinkErrors()
        const sidebar = new SidebarFragment()
        const viewDishesPage = new ViewDishesPage()
        const viewDishesErrors = new ViewDishesErrors()
        const viewDrinksPage = new ViewDrinksPage()
        const viewDrinksErrors = new ViewDrinksErrors()

        sidebar.getCreateDrinkTab().click()

        //Create dish
        createDrinkFragment.getNameInput().type(createDrinkConstants.randomName)
        createDrinkFragment.getDescriptionInput().type(createDrinkConstants.randomDescription)
        cy.uploadDrinkImage(createDrinkConstants.validFormatValidSizeFilePath)
        cy.enterNutrients('proteinsPer100Grams')
        cy.enterNutrients('fatsPer100Grams')
        cy.enterNutrients('carbsPer100Grams')
        cy.enterNutrients('fibersPer100Grams')
        cy.enterNutrients('caloriesPer100Grams')
        createDrinkFragment.getSubcategorySelector().select('Соки/Фреш')
        createDrinkFragment.getVitaminsSelector().select(2)
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getContentWrapper().screenshot({capture: "fullPage"})
        createDrinkErrors.getSuccessCreateDrinkMsg().should('be.visible')

        //Check for drink non-existence in "View Dish" tab
        sidebar.getViewDishesTab().click()
        viewDishesPage.getNameInput().type(createDrinkConstants.randomName)
        viewDishesPage.getSearchButton().click()
        viewDishesErrors.getNoRecordsFoundError().should('be.visible')
        viewDishesPage.getContentWrapper().screenshot({capture: "fullPage"})


        //Go to View Drinks page
        sidebar.getViewDrinkTab().click()
        viewDrinksPage.getNameInput().type(createDrinkConstants.randomName)
        viewDrinksPage.getSearchButton().click()
        viewDrinksPage.getTableBlock().get('td:nth-child(3)').should('be.visible').and('have.text', createDrinkConstants.randomName)
        viewDrinksPage.getTableBlock().screenshot({capture: "fullPage"})


        //Click on edit button
        cy.getEditButton(createDrinkConstants.randomName)
        createDrinkFragment.getCardBody().screenshot({capture: "fullPage"})
        createDrinkFragment.getNameInput().should('have.value', createDrinkConstants.randomName)
        createDrinkFragment.getDescriptionInput().should('have.value', createDrinkConstants.randomDescription)

        //Update drink values
        createDrinkFragment.getNameInput().clear().type(createDrinkConstants.updateRandomName)
        createDrinkFragment.getDescriptionInput().clear().type(createDrinkConstants.updateRandomDescription)
        cy.uploadDrinkImage(createDrinkConstants.secondValidFormatValidSizeFilePath)
        cy.enterNutrients('proteinsPer100Grams')
        cy.enterNutrients('fatsPer100Grams')
        cy.enterNutrients('carbsPer100Grams')
        cy.enterNutrients('fibersPer100Grams')
        cy.enterNutrients('caloriesPer100Grams')
        createDrinkFragment.getSubcategorySelector().select('Вода')
        createDrinkFragment.getVitaminsSelector().select('5')
        createDrinkFragment.getSubmitButton().click()
        createDrinkFragment.getContentWrapper().screenshot({capture: "fullPage"})
        createDrinkErrors.getSuccessUpdateDrinkMsg().should('be.visible')

        //Go to View Drinks page
        sidebar.getViewDrinkTab().click()
        viewDrinksPage.getNameInput().type(createDrinkConstants.updateRandomName)
        viewDrinksPage.getSearchButton().click()
        viewDrinksPage.getTableBlock().get('td:nth-child(3)').should('be.visible').and('have.text', createDrinkConstants.updateRandomName)
        viewDrinksPage.getTableBlock().get('td:nth-child(4)').should('be.visible').and('have.text', createDrinkConstants.updateRandomDescription)
        viewDrinksPage.getTableBlock().screenshot({capture: "fullPage"})

        //Delete dish
        cy.getDeleteButton(createDrinkConstants.updateRandomName)
        viewDrinksPage.getNameInput().type(createDrinkConstants.updateRandomName)
        viewDrinksPage.getSearchButton().click()
        viewDrinksErrors.getNoRecordsFoundError().should('be.visible')
        viewDrinksPage.getContentWrapper().screenshot({capture: "fullPage"})
    });


})