import {LoginConstants} from "../constants";
const {ViewDishesPage} = require("../support/page_objects/view_dishes/fragments");
const {ViewDishesErrors} = require("../support/page_objects/view_dishes/errors");
const {ViewDishesConstants} = require("../constants");
import { recurse } from 'cypress-recurse'


describe('View dishes', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Should check for presence of all fields on the page', () => {

        const viewDishesFragment = new ViewDishesPage()

        viewDishesFragment.getViewDishesTitle().should('be.visible')
        viewDishesFragment.getSearchBlock().should('be.visible')
        viewDishesFragment.getNameInput().should('be.visible')
        viewDishesFragment.getDescriptionInput().should('be.visible')
        viewDishesFragment.getOrderBySelector().should('be.visible')
        viewDishesFragment.getOrderSelector().should('be.visible')
        viewDishesFragment.getCategorySelector().should('be.visible')
        viewDishesFragment.getSubcategorySelector().should('be.visible')
        viewDishesFragment.getSpoonGramsInput().should('be.visible')
        viewDishesFragment.getSpoonGramsOption().should('be.visible')
        viewDishesFragment.getPalmGramsInput().should('be.visible')
        viewDishesFragment.getPalmGramsOption().should('be.visible')
        viewDishesFragment.getItemsPerPageSelector().should('be.visible')
        viewDishesFragment.getSearchButton().should('be.visible')
        viewDishesFragment.getTableBlock().should('be.visible')
        viewDishesFragment.getPaginationBlock().should('be.visible')
    })

    it('Should check validation errors', () => {

        const viewDishesFragment = new ViewDishesPage()
        const viewDishesErrors = new ViewDishesErrors()
        const viewDishesConstants = new ViewDishesConstants()


        //Name too long string check
        viewDishesFragment.getNameInput().type(viewDishesConstants.randomNameLargeString)
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getNameMaxError().should('be.visible')

        //Description too long string
        viewDishesFragment.getDescriptionInput().type(viewDishesConstants.randomDescriptionLargeString)
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getDescriptionMaxError().should('be.visible')

        //Grams in spoon min validation check (negative numbers)
        viewDishesFragment.getSpoonGramsInput().type('-1')
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getSpoonGramsMinError().should('be.visible')

        //Grams in spoon min validation check (0 value)
        viewDishesFragment.getSpoonGramsInput().type('0')
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getSpoonGramsMinError().should('be.visible')

        //Grams in palm min validation check (negative numbers)
        viewDishesFragment.getPalmGramsInput().type('-1')
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getPalmGramsMinError().should('be.visible')

        //Grams in palm min validation check (0 value)
        viewDishesFragment.getPalmGramsInput().type('0')
        viewDishesFragment.getSearchButton().click()
        viewDishesErrors.getPalmGramsMinError().should('be.visible')

        //Check Browser float validation message in "Grams in Spoon" and "Grams in Palm" fields
        cy.validateFloatBrowserErrorMsg('gramsInSpoon', 'Search')
        cy.validateFloatBrowserErrorMsg('gramsInPalm', 'Search')
    })


    it('Should find all dishes with `ж` element in name',  () =>{

        const viewDishesFragment = new ViewDishesPage()

        //Select "Fats" category
        viewDishesFragment.getNameInput().type('ж')
        viewDishesFragment.getOrderBySelector().select('Name')
        viewDishesFragment.getOrderSelector().select(1)
        viewDishesFragment.getItemsPerPageSelector().select('20')
        viewDishesFragment.getSearchButton().click()

        //BE returned dishes with `ж` element and sorted by ascending
        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            strings.forEach(el=>{
                cy.wrap(el).should('contain', 'ж');
            })
        })
    });

    it('Should find all dishes with `л` element in description',  () =>{

        const viewDishesFragment = new ViewDishesPage()

        //Select "Fats" category
        viewDishesFragment.getDescriptionInput().type('л')
        viewDishesFragment.getOrderBySelector().select('Description')
        viewDishesFragment.getOrderSelector().select(2)
        viewDishesFragment.getItemsPerPageSelector().select('20')
        viewDishesFragment.getSearchButton().click()

        //BE returned dishes with `ж` element and sorted by ascending
        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(4)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            strings.forEach(el=>{
                cy.wrap(el).should('contain', 'л');
            })
        })
    });


    it('Should find all dishes by selected category',  () =>{

        const viewDishesFragment = new ViewDishesPage()

        //Select "Fats" category
        viewDishesFragment.getCategorySelector().select('Fats')
        viewDishesFragment.getSearchButton().click()

        //BE returned dishes with only "Fats" category value
        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(5)')
            .each($el => {
                expect($el).to.have.text('Fats')
            })
    });


    it('Should find all dishes by selected subcategory',  () =>{

        const viewDishesFragment = new ViewDishesPage()

        //Select "Carbs" category and "Fruits" subcategory
        viewDishesFragment.getCategorySelector().select('Carbs')
        viewDishesFragment.getSubcategorySelector().select('Фрукты')
        viewDishesFragment.getSearchButton().click()

        //BE returned dishes with only "Carbs" category and "Fruits" subcategory value
        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(5)')
            .each($el => {
                expect($el).to.have.text('Carbs')
            })

        cy.get('table > tbody > tr > td:nth-child(6)')
            .each($el => {
                expect($el).to.have.text('Фрукты')
            })
    });

    it(' Should change number of displayed items based on "Items per page" option',  () => {

        //Table should display 20 items by default
        cy.get('table > tbody > tr').should('have.length', 20)

        //Change option of "Items per page" selector to 5
        cy.checkPagination(5)

        //Change option of "Items per page" selector to 10
        cy.checkPagination(10)

        //Change option of "Items per page" selector to 15
        cy.checkPagination(15)

        //Change option of "Items per page" selector to 20
        cy.checkPagination(20)

    });

    it(' Should check pagination',  () => {

        const viewDishesFragment = new ViewDishesPage()

        // const visitTextPageIfPossible = () => {
        //     cy.get('a[rel="next"]').then(($next) => {
        //         if ($next.hasClass('page-item disabled')) {
        //             // we are done - we are on the last page
        //             return
        //         }
        //
        //         cy.wait(500) // just for clarity
        //         cy.get('a[rel="next"]').click()
        //         visitTextPageIfPossible()
        //     })
        // }
        //
        // visitTextPageIfPossible()

                    recurse(
                        () => cy.get('a[rel="next"]'),
                        (next) => next.hasClass('page-item disabled'),
                        {
                            timeout: 60000,
                            delay: 1000, // just to make it clear what is going on
                            post () {
                                cy.get('a[rel="next"]').click()
                            },
                        }
                    )

        cy.log('**on the last page**')
        viewDishesFragment.getPaginationBlock().contains('li a', 8).should('be.visible')
        viewDishesFragment.getPaginationBlock().get('a[rel="next"]').should('have.class', 'page-item disabled')

    });

    it(' Should check sorting by name ascending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in ascending
        viewDishesFragment.getOrderSelector().select(1)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
                const strings = [...$elements].map(el => el.innerText)
                expect(strings).to.deep.equal([...strings].sort(function(a,b) {
                    a = a.toLowerCase();
                    b = b.toLowerCase();
                    if( a == b) return 0;
                    return a < b ? -1 : 1;
                }))
            })
    });


    it(' Should check sorting by name descending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getOrderSelector().select(2)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function (a, b) {
                return a.localeCompare(b);
            }).reverse())
        })

    });


    it(' Should check sorting by grams in palm ascending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getOrderBySelector().select('Grams In Palm')
        viewDishesFragment.getOrderSelector().select(1)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(9)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}))
        })

    });


    it(' Should check sorting by grams in palm descending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getOrderBySelector().select('Grams In Palm')
        viewDishesFragment.getOrderSelector().select(2)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(9)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}).reverse())
        })

    });


    it(' Should check sorting by grams in spoon ascending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getOrderBySelector().select('Grams In Spoon')
        viewDishesFragment.getOrderSelector().select(1)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(8)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}))
        })

    });


    it(' Should check sorting by grams in spoon descending',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getOrderBySelector().select('Grams In Spoon')
        viewDishesFragment.getOrderSelector().select(2)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(8)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}).reverse())
        })

    });


    it(' Should find all dishes with less or equal 30 grams in spoon',  () => {

        const viewDishesFragment = new ViewDishesPage()

        //Sort by name in descending
        viewDishesFragment.getSpoonGramsInput().type('30')
        viewDishesFragment.getSpoonGramsOption().select('Less or equals')
        viewDishesFragment.getOrderBySelector().select('Grams In Spoon')
        viewDishesFragment.getOrderSelector().select(2)
        cy.checkPagination(20)

        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(8)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            strings.forEach(el=>{
                cy.wrap(el).then(parseFloat).should('be.lte', 30);
            })
            expect(strings).to.deep.equal([...strings].sort(function(a,b){return a - b}).reverse())
        })

    });


    it(' Should display "No found" notification if there no matches',  () => {

        const viewDishesFragment = new ViewDishesPage()
        const viewDishesErrors = new ViewDishesErrors()
        const viewDishesConstants = new ViewDishesConstants()

        //Sort by name in descending
        viewDishesFragment.getNameInput().type(viewDishesConstants.randomName)
        viewDishesFragment.getSearchButton().click()
        viewDishesFragment.getTableBlock().screenshot({capture: "fullPage"})
        viewDishesErrors.getNoRecordsFoundError().should('be.visible')

    });






})