const {CreateDishPage} = require('../support/page_objects/create_dish/fragments')
const {CreateSubcategoryConstants, LoginConstants} = require("../constants");
const {SidebarFragment} = require("../support/page_objects/sidebar/fragments");
const {CreateSubcategoryPage} = require("../support/page_objects/create_subcategory/fragments");
const {CreateSubcategoryErrors} = require("../support/page_objects/create_subcategory/errors");
const {ViewDishesPage} = require("../support/page_objects/view_dishes/fragments");
const {CreateCategoryPage} = require("../support/page_objects/create_category/fragments");


describe('Create subcategory', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Modal check', () => {

        const sidebar = new SidebarFragment()
        const createSubcategoryFragment = new CreateSubcategoryPage()

        sidebar.getCreateSubcategoryTab().click()

        createSubcategoryFragment.getCreateSubcategoryTitle().should('be.visible')
        createSubcategoryFragment.getNameInput().should('be.visible')
        createSubcategoryFragment.getCategorySelector().should('be.visible')
        createSubcategoryFragment.getSubmitButton().should('be.visible')
        createSubcategoryFragment.getViewDishLink().should('be.visible')
    })

    it('Required errors check', () => {

        const sidebar = new SidebarFragment()
        const createSubcategoryFragment = new CreateSubcategoryPage()
        const createSubcategoryErrors = new CreateSubcategoryErrors()

        sidebar.getCreateSubcategoryTab().click()

        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryErrors.getNameRequiredError().should('be.visible')
        createSubcategoryErrors.getIconRequiredError().should('be.visible')

    })

    it('Validation errors check', () => {

        const sidebar = new SidebarFragment()
        const subcategoryConstant = new CreateSubcategoryConstants()
        const createSubcategoryFragment = new CreateSubcategoryPage()
        const createSubcategoryErrors = new CreateSubcategoryErrors()

        sidebar.getCreateSubcategoryTab().click()

        //Name too long string check
        createSubcategoryFragment.getNameInput().type(subcategoryConstant.randomNameLargeString)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getNameMaxError().should('be.visible')

        //Upload image with valid format and invalid size
        cy.uploadSubcategoryImage(subcategoryConstant.validFormatInvalidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getIconInvalidSizeError().should('be.visible')

        //Upload image with invalid format and valid size
        cy.uploadSubcategoryImage(subcategoryConstant.invalidFormatValidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getIconInvalidFormatError().should('be.visible')

        //Upload image with invalid format and invalid size
        cy.uploadSubcategoryImage(subcategoryConstant.invalidFormatInvalidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getIconInvalidFormatError().should('be.visible')

        //Check drag-n-drop function
        createSubcategoryFragment.getIconInput().selectFile(subcategoryConstant.validFormatValidSizeFilePath, {action: 'drag-drop'})
        createSubcategoryFragment.getSubmitButton().click()
    })


    it('Should display "This subcategory has already created" error after trying to create already existed subcategory',  () => {

        const sidebar = new SidebarFragment()
        const subcategoryConstant = new CreateSubcategoryConstants()
        const createSubcategoryFragment = new CreateSubcategoryPage()
        const createSubcategoryErrors = new CreateSubcategoryErrors()

        sidebar.getCreateSubcategoryTab().click()

        //Enter 'Яйца' in name input and choose "Proteins" category
        createSubcategoryFragment.getNameInput().type('Яйца')
        createSubcategoryFragment.getCategorySelector().select('Proteins')
        cy.uploadSubcategoryImage(subcategoryConstant.validFormatValidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()

        //Should display "This subcategory has already created" error
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getDuplicateSubcategoryError().should('be.visible')
    });


    it('Should create, update and delete subcategory',  () =>{

        const sidebar = new SidebarFragment()
        const subcategoryConstant = new CreateSubcategoryConstants()
        const createSubcategoryFragment = new CreateSubcategoryPage()
        const createCategoryFragment = new CreateCategoryPage()
        const createDishFragment = new CreateDishPage()
        const viewDishesFragment = new ViewDishesPage()
        const createSubcategoryErrors = new CreateSubcategoryErrors()

        sidebar.getCreateSubcategoryTab().click()

        //Create subcategory
        createSubcategoryFragment.getNameInput().type(subcategoryConstant.randomName)
        createSubcategoryFragment.getCategorySelector().select('Proteins')
        cy.uploadSubcategoryImage(subcategoryConstant.validFormatValidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})
        createSubcategoryErrors.getSuccessCreateSubcategoryMsg().should('be.visible')

        //Go to View Categories page
        sidebar.getViewCategoriesTab().click()
        cy.contains('Proteins').click()
        cy.contains(subcategoryConstant.randomName).should('be.visible')

        //Check for update on "Categories" selector

        //Go to "View Dishes" tab
        sidebar.getViewDishesTab().click()

        //Select 'Proteins' category
        viewDishesFragment.getCategorySelector().select('Proteins')
        cy.contains('select[name="subcategory"] option', subcategoryConstant.randomName, { matchCase: false })
        viewDishesFragment.getSubcategorySelector().select(subcategoryConstant.randomName)


        //Go to "Create dish" tab
        sidebar.getCreateDishTab().click()
        createDishFragment.getCategorySelector().select('Proteins')
        cy.contains('select[name="subcategory"] option', subcategoryConstant.randomName, { matchCase: false })
        createDishFragment.getSubcategorySelector().select(subcategoryConstant.randomName)
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})

        //Update subcategory
        sidebar.getViewCategoriesTab().click()
        cy.contains('Proteins').click()
        cy.getEditButton(subcategoryConstant.randomName)
        createSubcategoryFragment.getUpdateNameInput().should('have.value', subcategoryConstant.randomName)
        createSubcategoryFragment.getCategorySelector().find('option:selected').should('have.text', 'Proteins')
        createSubcategoryFragment.getIconInput().should('be.empty')

        //Update subcategory values
        createSubcategoryFragment. getUpdateNameInput().clear().type(subcategoryConstant.randomUpdateName)
        createSubcategoryFragment.getCategorySelector().select('Snacks')
        cy.uploadSubcategoryImage(subcategoryConstant.secondValidFormatValidSizeFilePath)
        createSubcategoryFragment.getSubmitButton().click()
        createSubcategoryErrors.getSuccessUpdateSubcategoryMsg().should('be.visible')

        //Go to View Categories page
        sidebar.getViewCategoriesTab().click()

        //Should not exist in Proteins category
        cy.contains('Proteins').click()
        cy.contains(subcategoryConstant.randomUpdateName).should('not.be.visible')

        //Should be visible in Snacks category
        cy.contains('Snacks').click()
        cy.contains(subcategoryConstant.randomUpdateName).should('be.visible')

        //Go to "View Dishes" tab
        sidebar.getViewDishesTab().click()

        //Should be updated in "View Dishes" tab
        viewDishesFragment.getCategorySelector().select('Snacks')
        cy.contains('select[name="subcategory"] option', subcategoryConstant.randomUpdateName, { matchCase: false })
        viewDishesFragment.getSubcategorySelector().select(subcategoryConstant.randomUpdateName)

        //Should be updated in  "Create dish" tab
        sidebar.getCreateDishTab().click()
        createDishFragment.getCategorySelector().select('Snacks')
        cy.contains('select[name="subcategory"] option', subcategoryConstant.randomUpdateName, { matchCase: false })
        createDishFragment.getSubcategorySelector().select(subcategoryConstant.randomUpdateName)
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})

        //Delete subcategory
        sidebar.getViewCategoriesTab().click()
        cy.contains('Snacks').click()
        cy.getDeleteButton(subcategoryConstant.randomUpdateName)

        //Should be removed from "Snacks" category
        cy.contains('Snacks').click()
        cy.contains(subcategoryConstant.randomUpdateName).should('not.exist')

        //Go to "View dishes" tab
        sidebar.getViewDishesTab().click()

        //Should be removed from "Subcategory" selector
        viewDishesFragment.getCategorySelector().select('Snacks')
        cy.get(`select[id="subcategory"] option:contains(${subcategoryConstant.randomUpdateName})`).should('not.exist')


        //Go to "Create dish" tab
        sidebar.getCreateDishTab().click()

        //Should be removed from "Subcategory" selector
        createDishFragment.getCategorySelector().select('Snacks')
        cy.get(`select[id="subcategory"] option:contains(${subcategoryConstant.randomUpdateName})`).should('not.exist')

    });


})