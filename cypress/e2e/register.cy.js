const {LoginConstants, RegisterConstants} = require('../constants')
const {LoginPage} = require('../support/page_objects/login/fragments')
const {RegisterPage} = require("../support/page_objects/register/fragments");
const {RegisterErrors} = require("../support/page_objects/register/errors");

describe('Register', () => {

    beforeEach(()=>{
        cy.visit('/login')
    })

    it('Should check all fields to be visible', () => {

        const registerPage = new RegisterPage()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterBox().should('be.visible')
        registerPage.getFullNameInput().should('be.visible')
        registerPage.getEmailInput().should('be.visible')
        registerPage.getPasswordInput().should('be.visible')
        registerPage.getPasswordConfirmInput().should('be.visible')
        registerPage.getRegisterButton().should('be.visible')
        registerPage.getLoginLink().should('be.visible')
    })


    it('Should register user and allow him to login with valid credentials', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const loginPage = new LoginPage()
        const registerConstants = new RegisterConstants()

        loginPage.getRegisterLink().click()

        //Create new user
        registerPage.getFullNameInput().type(registerConstants.randomValidName)
        registerPage.getEmailInput().type(registerConstants.randomValidDomainEmail)
        registerPage.getPasswordInput().type(registerConstants.randomValidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomValidPassword)
        registerPage.getRegisterButton().click()

        //User get redirected to the Login modal
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        registerErrors.getSuccessRegisterMsg().should('be.visible')
        loginPage.getLoginBox().should('be.visible')
        loginPage.getEmailInput().should('be.visible')
        loginPage.getPasswordInput().should('be.visible')
        loginPage.getRememberMeLabel().should('be.visible')
        loginPage.getSignInButton().should('be.visible')
        loginPage.getForgotPasswordLink().should('be.visible')
        loginPage.getRegisterLink().should('be.visible')


        //Login with credentials of newly created user
        cy.typeEmailAndPassword(registerConstants.randomValidDomainEmail, registerConstants.randomValidPassword)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        cy.getCookie('laravel_session').should('exist')
    })

    it('Should display required fields notifications', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getNameRequiredError().should('be.visible')
        registerErrors.getEmailRequiredError().should('be.visible')
        registerErrors.getPasswordRequiredError().should('be.visible')
    })

    it('Should display "The name must not be greater than 32 characters" with less than more than 33 letters full name', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getFullNameInput().type(registerConstants.randomMaxInvalidName)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getNameMaxError().should('be.visible')
    })


    it('Should display "The name must be at least 2 characters." with less than 2 letters full name', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getFullNameInput().clear().type(registerConstants.randomMinInvalidName)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getNameMinError().should('be.visible')
    })


    it('Should display "Your E-mail is incorrect, please use given email "example@portion.club"" with invalid domain email', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getEmailInput().clear().type(registerConstants.randomInvalidDomainEmail)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getEmailInvalidDomainError().should('be.visible')

    })


    it('Should display "The email must be a valid email address." with invalid email format', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getEmailInput().clear().type(registerConstants.randomInvalidFormatValidDomainEmail)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getEmailInvalidFormatError().should('be.visible')
    })

    it('Should display "The email must be a valid email address." with invalid format and domain email', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getEmailInput().clear().type(registerConstants.randomInvalidFormatInvalidDomainEmail)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getEmailInvalidFormatError().should('be.visible')
    })


    it('Should display " Your E-mail is incorrect, please use given email "example@portion.club" " with less than 2 symbols before @ email', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getEmailInput().clear().type(registerConstants.randomMinInvalidEmail)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getEmailInvalidDomainError().should('be.visible')
    })


    it('Should display "The email must not be greater than 32 characters." with more than 32 symbols before @ email', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getEmailInput().clear().type(registerConstants.randomMaxInvalidEmail)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getEmailMaxError().should('be.visible')
    })


    it('Should display "The password must be at least 8 characters." with less than 8 symbols password', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomMinInvalidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomMinInvalidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordMinError().should('be.visible')
    })


    it('Should display "The password must not be greater than 16 characters.', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomMaxInvalidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomMaxInvalidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordMaxError().should('be.visible')
    })


    it('Should display "The password must contain at least one uppercase and one lowercase letter." without uppercase letters in password', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomUpperInvalidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomUpperInvalidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordLettersError().should('be.visible')
    })


    it('Should display "The password must contain at least one uppercase and one lowercase letter." without lowercase letters in password', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomLowerInvalidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomLowerInvalidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordLettersError().should('be.visible')
    })


    it('Should display "The password must contain at least one uppercase and one lowercase letter." without numbers in password', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomNumInvalidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomNumInvalidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordNumbersError().should('be.visible')
    })

    it('Should display "The password confirmation does not match." with different confirmed password', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getPasswordInput().type(registerConstants.randomValidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.invalidConfirmedPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getPasswordConfirmError().should('be.visible')
    })

    it('Should display "This e-mail already exists, try another one" with already registered email', () => {

        const registerPage = new RegisterPage()
        const registerErrors = new RegisterErrors()
        const registerConstants = new RegisterConstants()
        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()

        loginPage.getRegisterLink().click()

        //Enter already existed email (for example, 'vlad.google@portion.club')
        registerPage.getFullNameInput().type(registerConstants.randomValidName)
        registerPage.getEmailInput().type(loginConstants.emailForLogin)
        registerPage.getPasswordInput().type(registerConstants.randomValidPassword)
        registerPage.getPasswordConfirmInput().type(registerConstants.randomValidPassword)
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterButton().click()

        //Should display "This e-mail already exists, try another one" error
        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerErrors.getExistedEmailError().should('be.visible')
    })


    it('Should redirect to the login page after clicking on login link', () => {

        const registerPage = new RegisterPage()
        const loginPage = new LoginPage()

        loginPage.getRegisterLink().click()
        registerPage.getLoginLink().click()

        loginPage.getLoginBox().should('be.visible')
        loginPage.getEmailInput().should('be.visible')
        loginPage.getPasswordInput().should('be.visible')
        loginPage.getRememberMeLabel().should('be.visible')
        loginPage.getSignInButton().should('be.visible')
        loginPage.getForgotPasswordLink().should('be.visible')
        loginPage.getRegisterLink().should('be.visible')
    })




})