const {CreateDishPage} = require('../support/page_objects/create_dish/fragments')
const {ResetPasswordConstants} = require("../constants/resetPassword.constants");
const {CreateCategoryConstants} = require('../constants/createCategory.constants')
const {SidebarFragment} = require("../support/page_objects/sidebar/fragments");
const {CreateSubcategoryPage} = require("../support/page_objects/create_subcategory/fragments");
const {CreateCategoryPage} = require('../support/page_objects/create_category/fragments')
const {CreateCategoryErrors} = require('../support/page_objects/create_category/errors')
const {ViewDishesPage} = require("../support/page_objects/view_dishes/fragments");
const {LoginConstants} = require("../constants");


describe('Create category', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Modal check', () => {

        const constant = new ResetPasswordConstants()
        const createCategoryFragment = new CreateCategoryPage()
        const sidebar = new SidebarFragment()

        //Log in
        cy.login(constant.emailForLogin, constant.passwordForLogin)
        sidebar.getCreateCategoryTab().click()

        //Check modal

        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryFragment.getCreateCategoryTitle().should('be.visible')
        createCategoryFragment.getNameInput().should('be.visible')
        createCategoryFragment.getSubmitButton().should('be.visible')
        createCategoryFragment.getViewDishLink().should('be.visible')
    })


    it('Required and validation errors check', () => {

        const createCategoryConstant = new CreateCategoryConstants()
        const sidebar = new SidebarFragment()
        const createCategoryFragment = new CreateCategoryPage()
        const createCategoryErrors = new CreateCategoryErrors()

        sidebar.getCreateCategoryTab().click()

        //Required error check
        createCategoryFragment.getSubmitButton().click()
        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryErrors.getNameRequiredError().should('be.visible')

        //Validation error check
        createCategoryFragment.getNameInput().type(createCategoryConstant.randomNameLargeString)
        createCategoryFragment.getSubmitButton().click()
        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryErrors.getNameMaxError().should('be.visible')

    })


    it('Should display "This category has already created" error after trying to create already existed category',  () => {

        const sidebar = new SidebarFragment()
        const createCategoryFragment = new CreateCategoryPage()
        const createCategoryErrors = new CreateCategoryErrors()

        sidebar.getCreateCategoryTab().click()

        //Enter 'Fats' in name input
        createCategoryFragment.getNameInput().type('Fats')
        createCategoryFragment.getSubmitButton().click()
        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryErrors.getDuplicateCategoryError().should('be.visible')
    });


    it('Should create, update and delete category',  () =>{

        const createCategoryConstant = new CreateCategoryConstants()
        const sidebar = new SidebarFragment()
        const createCategoryFragment = new CreateCategoryPage()
        const createSubcategoryFragment = new CreateSubcategoryPage()
        const createDishFragment = new CreateDishPage()
        const viewDishesFragment = new ViewDishesPage()
        const createCategoryErrors = new CreateCategoryErrors()

        sidebar.getCreateCategoryTab().click()

        //Create category
        createCategoryFragment.getNameInput().type(createCategoryConstant.randomName)
        createCategoryFragment.getSubmitButton().click()
        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryErrors.getSuccessCreateCategoryMsg().should('be.visible')

        //Go to View Categories page
        sidebar.getViewCategoriesTab().click()
        cy.contains(createCategoryConstant.randomName).should('be.visible')

        //Check for update on "Categories" selector

        //Go to "View Dishes" tab
        sidebar.getViewDishesTab().click()

        //Select 'Proteins' category
        cy.contains('select[name="category"] option', createCategoryConstant.randomName)
        viewDishesFragment.getCategorySelector().select(createCategoryConstant.randomName)
        viewDishesFragment.getSearchBlock().screenshot({capture: "fullPage"})

        //Go to "Create dish" tab
        sidebar.getCreateDishTab().click()
        cy.contains('select[name="category"] option', createCategoryConstant.randomName)
        createDishFragment.getCategorySelector().select(createCategoryConstant.randomName)
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})


        //Go to "Create subcategory" tab
        sidebar.getCreateSubcategoryTab().click()
        cy.contains('select[name="category"] option', createCategoryConstant.randomName, { matchCase: false })
        createSubcategoryFragment.getCategorySelector().select(createCategoryConstant.randomName)
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})

        //Update category
        sidebar.getViewCategoriesTab().click()
        cy.getEditButton(createCategoryConstant.randomName)
        createCategoryFragment.getNameInput().type(`{selectAll}{backspace}${createCategoryConstant.randomUpdateName}`)
        createCategoryFragment.getSubmitButton().click()
        createCategoryFragment.getCreateCategoryCard().screenshot({capture: "fullPage"})
        createCategoryErrors.getSuccessUpdateCategoryMsg().should('be.visible')


        //Go to View Categories page
        sidebar.getViewCategoriesTab().click()
        cy.contains(createCategoryConstant.randomUpdateName).should('be.visible')

        //Check for update on "Categories" selector

        //Go to "View Dishes" tab
        sidebar.getViewDishesTab().click()

        //Select newly updated category
        cy.contains('select[name="category"] option', createCategoryConstant.randomUpdateName)
        viewDishesFragment.getCategorySelector().select(createCategoryConstant.randomUpdateName)
        viewDishesFragment.getSearchBlock().screenshot({capture: "fullPage"})

        //Go to "Create dish" tab
        sidebar.getCreateDishTab().click()
        cy.contains('select[name="category"] option', createCategoryConstant.randomUpdateName)
        createDishFragment.getCategorySelector().select(createCategoryConstant.randomUpdateName)
        createDishFragment.getCardBody().screenshot({capture: "fullPage"})


        //Go to "Create subcategory" tab
        sidebar.getCreateSubcategoryTab().click()
        cy.contains('select[name="category"] option', createCategoryConstant.randomUpdateName, { matchCase: false })
        createSubcategoryFragment.getCategorySelector().select(createCategoryConstant.randomUpdateName)
        createSubcategoryFragment.getCreateSubcategoryCard().screenshot({capture: "fullPage"})


        //Delete category
        sidebar.getViewCategoriesTab().click()
        cy.getDeleteButton(createCategoryConstant.randomUpdateName)
        cy.contains(createCategoryConstant.randomUpdateName).should('not.exist')


        //Check for non-existence in "Category" selector

        //Go to "View Dishes" tab
        sidebar.getViewDishesTab().click()
        cy.get(`select[id="category"] option:contains(${createCategoryConstant.randomUpdateName})`).should('not.exist')


        //Go to "Create dish" tab
        sidebar.getCreateDishTab().click()
        cy.get(`select[name="category"] option:contains(${createCategoryConstant.randomUpdateName})`).should('not.exist')


        //Go to "Create subcategory" tab
        sidebar.getCreateSubcategoryTab().click()
        cy.get(`select[id="category"] option:contains(${createCategoryConstant.randomUpdateName})`).should('not.exist')

    });


})