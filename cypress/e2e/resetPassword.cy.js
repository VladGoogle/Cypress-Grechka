const {ResetPasswordPage} = require('../support/page_objects/reset_password/fragments')
const {ResetPasswordErrors} = require('../support/page_objects/reset_password/errors')
const {ResetPasswordConstants} = require("../constants/resetPassword.constants");
const {LoginPage} = require("../support/page_objects/login/fragments");


describe('Reset password', () => {

  before(()=>{
    cy.visit('/login')
  })

  it('Reset password', () => {

    const resetPasswordPage = new ResetPasswordPage()
    const loginPage = new LoginPage()
    const error = new ResetPasswordErrors()
    const resetPasswordConstants = new ResetPasswordConstants()

    loginPage.getForgotPasswordLink().click()

    resetPasswordPage.getLoginBox().should('be.visible')
    resetPasswordPage.getLoginLogo().should('be.visible').and('contain.text', 'Proper Eating')
    resetPasswordPage.getLoginBoxMessage().should('be.visible').and('have.text', 'You forgot your password? Here you can easily retrieve a new password.')
    resetPasswordPage.getEmail().should('be.visible')
    resetPasswordPage.getSubmitButton().should('be.visible').and('have.text', 'Request new password')
    resetPasswordPage.getLoginLink().should('be.visible').and('have.text', 'Login')
    resetPasswordPage.getRegisterLink().should('be.visible').and('have.text', 'Register a new membership')

    resetPasswordPage.getEmail().type(resetPasswordConstants.randomInvalidFormatEmail)
    resetPasswordPage.getSubmitButton().click()
    error.getInvalidLoginError().should('be.visible')
  })

})