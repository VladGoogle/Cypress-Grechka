
describe('API testing', () => {

    it('fetches  all Categories - GET', () => {
        cy.request('/api/category').then((res) => {
            expect(res.status).to.eq(200);
            assert.isArray(res.body, "Categories body is array")

        });
    })


    it('fetches one Category - GET', () => {
        cy.request(`/api/category/6`).then((res) => {
            expect(res.status).to.eq(200);
            expect(res.body).to.have.property('subcategories')
            cy.wrap(res.body).should('include', {
                id: 6,
                name: "Drink"
            });
        });
    })


    it('fetches all Subcategories - GET', () => {
        cy.request(`/api/subcategory`).then((res) => {
            expect(res.status).to.eq(200);
            assert.isArray(res.body, "Subcategories body is array")
        });
    })


    it('fetches all Subcategories for drinks - GET', () => {
        cy.request(`/api/subcategory/subcategoriesList`).then((res) => {
            expect(res.status).to.eq(200);
            assert.isArray(res.body, "Subcategories for drinks body is array")
            expect(res.body).to.have.length(2)
            expect(res.body[0].name).to.eq('Вода')
            expect(res.body[1].name).to.eq('Соки/Фреш')
        });
    })


    it('fetches one Subcategory - GET', () => {
        cy.request(`/api/subcategory/29`).then((res) => {
            expect(res.status).to.eq(200);
            cy.wrap(res.body).should('include', {
                id: 29,
                name: "Ягоды"
            });
        });
    })


    it('fetches all Dishes - GET', () => {
        cy.request(`/api/dish`).then((res) => {
            expect(res.status).to.eq(200);
            assert.isArray(res.body, "Dishes body is array")
        });
    })


    it('fetches one Dish - GET', () => {
        cy.request(`/api/dish/1`).then((res) => {
            expect(res.status).to.eq(200);
            expect(res.body).to.have.property('plateImage')
            expect(res.body).to.have.property('searchImage')
            expect(res.body).to.have.property('subcategoryId')
            cy.wrap(res.body).should('include', {
                id: 1,
                name: "Цветная капуста"
            });
        });
    })


    it('fetches Dishes by Category Id - GET', () => {
        cy.request(`/api/dish/getDishesByCategoryId/1`).then((res) => {
            expect(res.status).to.eq(200);
            assert.isArray(res.body, "Dishes body is array")
            cy.wrap(res.body).each((item)=>{
                expect(item).to.include({
                    category: "Fats"
                })
            })
        })
    })




})