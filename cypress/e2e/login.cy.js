const {ResetPasswordPage} = require('../support/page_objects/reset_password/fragments')
const {LoginConstants, RegisterConstants} = require('../constants')
const {LoginPage} = require('../support/page_objects/login/fragments')
const {LoginErrors} = require('../support/page_objects/login/errors')
const {RegisterPage} = require("../support/page_objects/register/fragments");
const {RegisterErrors} = require("../support/page_objects/register/errors");

describe('Login', () => {

    beforeEach(()=>{
        cy.visit('/login')
    })

    it('Should check all fields to be visible', () => {

        const loginPage = new LoginPage()

        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getLoginBox().should('be.visible')
        loginPage.getLoginLogo().should('be.visible')
        loginPage.getEmailInput().should('be.visible')
        loginPage.getPasswordInput().should('be.visible')
        loginPage.getRememberMeLabel().should('be.visible')
        loginPage.getSignInButton().should('be.visible')
        loginPage.getForgotPasswordLink().should('be.visible')
        loginPage.getRegisterLink().should('be.visible')
    })


    it('Should allow user to enter admin panel with valid login and password', () => {

        const loginConstants = new LoginConstants()
        cy.login(loginConstants.emailForLogin, loginConstants.passwordForLogin)
        cy.getCookie('laravel_session').should('exist')

    })

    it('Should display required fields notifications', () => {

        const loginPage = new LoginPage()
        const loginErrors = new LoginErrors()

        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getEmailRequiredError().should('be.visible')
        loginErrors.getPasswordRequiredError().should('be.visible')
    })

    it('Should display "Invalid creds" with incorrect email and incorrect password', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomValidFormatEmail, loginConstants.randomPassword)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidCredError().should('be.visible')
    })

    it('Should display "Invalid creds" with correct email and incorrect password', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.emailForLogin, loginConstants.randomPassword)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidCredError().should('be.visible')
    })

    it('Should display "Invalid creds" with incorrect email and correct password', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomValidFormatEmail, loginConstants.passwordForLogin)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidCredError().should('be.visible')
    })

    it('Should display "Your E-mail is incorrect, please use given email "example@portion.club" " with incorrect domain', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomInvalidDomainEmail, loginConstants.passwordForLogin)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getIncorrectEmailError().should('be.visible')
    })

    it('Should display "Invalid credentials" with invalid format and valid domain', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomInvalidFormatValidDomainEmail, loginConstants.passwordForLogin)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidEmailError().should('be.visible')
    })

    it('Should display "Invalid credentials" with invalid format and invalid domain', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomInvalidFormatInvalidDomainEmail, loginConstants.passwordForLogin)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidEmailError().should('be.visible')
    })


    it('Should display "Invalid credentials" with valid format and invalid domain', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const loginErrors = new LoginErrors()

        cy.typeEmailAndPassword(loginConstants.randomInvalidDomainEmail, loginConstants.passwordForLogin)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getIncorrectEmailError().should('be.visible')
    })

    it('Should redirect to the Reset Password page after clicking on "Forgot Password" link', () => {

        const loginPage = new LoginPage()
        const resetPasswordPage = new ResetPasswordPage()

        loginPage.getForgotPasswordLink().click()

        resetPasswordPage.getLoginBox().screenshot({capture: "fullPage"})
        resetPasswordPage.getLoginBox().should('be.visible')
        resetPasswordPage.getLoginLogo().should('be.visible')
        resetPasswordPage.getLoginBoxMessage().should('be.visible')
        resetPasswordPage.getEmail().should('be.visible')
        resetPasswordPage.getSubmitButton().should('be.visible')
        resetPasswordPage.getLoginLink().should('be.visible')
        resetPasswordPage.getRegisterLink().should('be.visible')
    })

    it('Should redirect to the Register page after clicking on register link', () => {

        const loginPage = new LoginPage()
        const registerPage = new RegisterPage()

        loginPage.getRegisterLink().click()

        registerPage.getRegisterBox().screenshot({capture: "fullPage"})
        registerPage.getRegisterBox().should('be.visible')
        registerPage.getFullNameInput().should('be.visible')
        registerPage.getEmailInput().should('be.visible')
        registerPage.getPasswordInput().should('be.visible')
        registerPage.getPasswordConfirmInput().should('be.visible')
        registerPage.getRegisterButton().should('be.visible')
        registerPage.getLoginLink().should('be.visible')
    })

    it('Should show "Invalid credentials" error after failing password min validation (FIT-94)', () => {

        const loginPage = new LoginPage()
        const loginConstants = new LoginConstants()
        const registerConstants = new RegisterConstants()
        const loginErrors = new LoginErrors()
        const registerErrors = new RegisterErrors()


        cy.typeEmailAndPassword(loginConstants.emailForLogin , registerConstants.randomMinInvalidPassword)
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginPage.getSignInButton().click()
        loginPage.getLoginBox().screenshot({capture: "fullPage"})
        loginErrors.getInvalidCredError().should('be.visible')
        registerErrors.getPasswordMinError().should('not.exist')

    })





})