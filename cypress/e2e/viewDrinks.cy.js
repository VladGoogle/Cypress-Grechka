
const {SidebarFragment} = require("../support/page_objects/sidebar/fragments");
const {ViewDrinksPage} = require("../support/page_objects/view_drinks/fragments");
const {ViewDrinksErrors} = require("../support/page_objects/view_drinks/errors");
const {ViewDrinksConstants, LoginConstants} = require("../constants");



describe('View drinks', () => {

    beforeEach(()=>{
        const constant = new LoginConstants()
        cy.login(constant.emailForLogin, constant.passwordForLogin)
    })

    it('Should check for presence of all fields on the page', () => {

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        viewDrinksFragment.getViewDrinksTitle().should('be.visible')
        viewDrinksFragment.getSearchBlock().should('be.visible')
        viewDrinksFragment.getNameInput().should('be.visible')
        viewDrinksFragment.getDescriptionInput().should('be.visible')
        viewDrinksFragment.getOrderBySelector().should('be.visible')
        viewDrinksFragment.getOrderSelector().should('be.visible')
        viewDrinksFragment.getSubcategorySelector().should('be.visible')
        viewDrinksFragment.getItemsPerPageSelector().should('be.visible')
        viewDrinksFragment.getSearchButton().should('be.visible')
        viewDrinksFragment.getTableBlock().should('be.visible')
        //viewDrinksFragment.getPaginationBlock().should('be.visible')
    })


    it('Should check validation errors', () => {

        const viewDrinksFragment = new ViewDrinksPage()
        const viewDrinksErrors = new ViewDrinksErrors()
        const viewDrinksConstants = new ViewDrinksConstants()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Name too long string check
        viewDrinksFragment.getNameInput().type(viewDrinksConstants.randomNameLargeString)
        viewDrinksFragment.getSearchButton().click()
        viewDrinksFragment.getSearchBlock().screenshot({capture: "fullPage"})
        viewDrinksErrors.getNameMaxError().should('be.visible')

        //Description too long string
        viewDrinksFragment.getDescriptionInput().type(viewDrinksConstants.randomDescriptionLargeString)
        viewDrinksFragment.getSearchButton().click()
        viewDrinksFragment.getSearchBlock().screenshot({capture: "fullPage"})
        viewDrinksErrors.getDescriptionMaxError().should('be.visible')

    })


    it('Should find all drinks with `i` element in name',  () =>{

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Select "Fats" category
        viewDrinksFragment.getNameInput().type('i')
        viewDrinksFragment.getOrderBySelector().select('Name')
        viewDrinksFragment.getOrderSelector().select(1)
        viewDrinksFragment.getItemsPerPageSelector().select('20')
        viewDrinksFragment.getSearchButton().click()

        //BE returned dishes with `a` element and sorted by ascending
        viewDrinksFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            strings.forEach(el=>{
                // let elem = el.toLowerCase()
                cy.wrap(el).should('contain', 'i');
            })
        })
    });


    it('Should find all drinks with `t` element in description',  () =>{

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        viewDrinksFragment.getDescriptionInput().type('t')
        viewDrinksFragment.getOrderBySelector().select('Description')
        viewDrinksFragment.getOrderSelector().select(2)
        viewDrinksFragment.getItemsPerPageSelector().select('20')
        viewDrinksFragment.getSearchButton().click()

        //BE returned dishes with `t` element and sorted by ascending
        viewDrinksFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(4)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            strings.forEach(el=>{
                let elem = el.toLowerCase()
                cy.wrap(elem).should('contain', 't');
            })
        })
    });


    it('Should find all drinks by selected subcategory',  () =>{

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Select  "Соки/Фреш" subcategory
        viewDrinksFragment.getSubcategorySelector().select('Соки/Фреш')
        viewDrinksFragment.getSearchButton().click()

        //BE returned drinks with only "Соки/Фреш" subcategory value
        viewDrinksFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(5)')
            .each($el => {
                expect($el).to.have.text('Соки/Фреш')
            })
    });

    // it(' Should change number of displayed items based on "Items per page" option',  () => {
    //
    //     const constant = new ResetPasswordConstants()
    //
    //     cy.login(constant.emailForLogin, constant.passwordForLogin)
    //
    //     //Table should display 20 items by default
    //     cy.get('table > tbody > tr').should('have.length', 20)
    //
    //     //Change option of "Items per page" selector to 5
    //     cy.checkPagination(5)
    //
    //     //Change option of "Items per page" selector to 10
    //     cy.checkPagination(10)
    //
    //     //Change option of "Items per page" selector to 15
    //     cy.checkPagination(15)
    //
    //     //Change option of "Items per page" selector to 20
    //     cy.checkPagination(20)
    //
    // });

    // it(' Should check pagination',  () => {
    //
    //     const constant = new ResetPasswordConstants()
    //     const viewDrinksFragment = new ViewDrinksPage()
    //     const sidebar = new SidebarFragment()
    //
    //     cy.login(constant.emailForLogin, constant.passwordForLogin)
    //     sidebar.getViewDrinkTab().click()
    //
    //     //goes to the last page
    //     const visitTextPageIfPossible = () => {
    //         viewDrinksFragment.getPaginationBlock().get('a[rel="next"]').then(($next) => {
    //             if ($next.hasClass('page-item disabled')) {
    //                 // we are done - we are on the last page
    //                 return
    //             }
    //
    //             cy.wait(500) // just for clarity
    //             viewDrinksFragment.getPaginationBlock().get('a[rel="next"]').click()
    //             visitTextPageIfPossible()
    //         })
    //     }
    //
    //     visitTextPageIfPossible()
    //     cy.log('**on the last page**')
    //     viewDrinksFragment.getPaginationBlock().contains('li a', 9).should('be.visible')
    //     viewDrinksFragment.getPaginationBlock().get('a[rel="next"]').should('have.class', 'page-item disabled')
    //
    // });

    it(' Should check sorting by name ascending',  () => {

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Sort by name in ascending
        viewDrinksFragment.getOrderBySelector().select('Name')
        viewDrinksFragment.getOrderSelector().select(1)
        viewDrinksFragment.getSearchButton().click()

        viewDrinksFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function (a,b) {
                a = a.toLowerCase();
                b = b.toLowerCase();
                if( a == b) return 0;
                return a < b ? -1 : 1;
            }))
        })
    });


    it(' Should check sorting by name descending',  () => {

        const viewDrinksFragment = new ViewDrinksPage()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Sort by name in descending
        viewDrinksFragment.getOrderBySelector().select('Name')
        viewDrinksFragment.getOrderSelector().select(2)
        viewDrinksFragment.getSearchButton().click()

        viewDrinksFragment.getTableBlock().screenshot({capture: "fullPage"})
        cy.get('table > tbody > tr > td:nth-child(3)').then($elements => {
            const strings = [...$elements].map(el => el.innerText)
            expect(strings).to.deep.equal([...strings].sort(function (a,b) {
                a = a.toLowerCase();
                b = b.toLowerCase();
                if( a == b) return 0;
                return a < b ? -1 : 1;
            }).reverse())
        })
    });

    it(' Should display "No found" notification if there no matches',  () => {

        const viewDrinksFragment = new ViewDrinksPage()
        const viewDrinksErrors = new ViewDrinksErrors()
        const viewDrinksConstants = new ViewDrinksConstants()
        const sidebar = new SidebarFragment()

        sidebar.getViewDrinkTab().click()

        //Sort by name in descending
        viewDrinksFragment.getNameInput().type(viewDrinksConstants.randomName)
        viewDrinksFragment.getSearchButton().click()
        viewDrinksFragment.getBody().screenshot({capture: "fullPage"})
        viewDrinksErrors.getNoRecordsFoundError().should('be.visible')

    });

})