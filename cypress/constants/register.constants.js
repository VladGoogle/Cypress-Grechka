const chance = require('chance').Chance();
const generator = require('generate-password');
const {StringGenerator} = require('../middlewares/generators')

class RegisterConstants {
    constructor() {
        this.stringGenerator = new StringGenerator()
        this.randomValidName = chance.name()
        this.randomMinInvalidName = chance.string({length: 1})
        this.randomMaxInvalidName = chance.string({length: 33})
        this.randomMinInvalidEmail = chance.string({length: 1, pool: 'abcde'}) + '@portion.club'
        this.randomMaxInvalidEmail = chance.string({length: 33, pool: 'abcde'}) + '@portion.club'
        this.randomValidDomainEmail = chance.email({domain: 'portion.club'})
        this.randomInvalidDomainEmail = chance.email({domain: 'gmail.com'})
        this.randomInvalidFormatValidDomainEmail = '.' + chance.email({domain: 'portion.club'})
        this.randomInvalidFormatInvalidDomainEmail = '.' + chance.email({domain: 'gmail.com'})
        this.randomValidPassword = this.stringGenerator.generateUsualString(16)

        this.randomMinInvalidPassword = generator.generate({
            length: 7,
            numbers: true,
            symbols: true
        })

        this.randomMaxInvalidPassword = generator.generate({
            length: 17,
            numbers: true,
            symbols: true
        })

        this.randomUpperInvalidPassword = generator.generate({
            length: 8,
            uppercase: false,
            numbers: true,
            symbols: true
        })

        this.randomLowerInvalidPassword = generator.generate({
            length: 8,
            lowercase: false,
            numbers: true,
            symbols: true
        })

        this.randomNumInvalidPassword = generator.generate({
            length: 8,
            numbers: false,
            symbols: false
        })

        this.onlyLowerPassword = generator.generate({
            length:8,
            uppercase: false,
            numbers: false,
            symbols: false,
        })

        this.onlyUppertersPassword = generator.generate({
            length:8,
            lowercase: false,
            numbers: false,
            symbols: false,
        })

        this.onlyNumbersPassword = this.stringGenerator.generateNumberString(8)
        this.onlySymbolsPassword = this.stringGenerator.generateSymbolString(8)


        this.invalidConfirmedPassword = generator.generate({
            length: 8,
            numbers: true,
            symbols: true
        })
    }
}

module.exports = {
    RegisterConstants
}