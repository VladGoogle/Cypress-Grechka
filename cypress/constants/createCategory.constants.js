const chance = require('chance').Chance();
const {StringGenerator} = require('../middlewares/generators')

class CreateCategoryConstants {
    constructor() {
        this.stringGenerator = new StringGenerator()
        this.randomName = this.stringGenerator.generateUpperString(30)
        this.randomUpdateName = this.stringGenerator.generateUpperString(30)
        this.randomNameLargeString = chance.string({length: 31})

    }
}

module.exports = {
    CreateCategoryConstants
}