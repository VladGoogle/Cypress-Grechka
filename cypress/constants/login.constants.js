const chance = require('chance').Chance();
const generator = require('generate-password');

class LoginConstants {
    constructor() {
        this.emailForLogin = 'vlad.google@portion.club'
        this.passwordForLogin = 'Asdrtyjklmnb1@'
        this.randomValidFormatEmail = chance.email({domain: 'portion.club'})
        this.randomInvalidFormatValidDomainEmail = '.' + chance.email({domain: 'portion.club'})
        this.randomInvalidDomainEmail = chance.email({domain: 'gmail.com'})
        this.randomInvalidFormatInvalidDomainEmail = '.' + chance.email({domain: 'gmail.com'})
        this.randomPassword = generator.generate({
            length: 8,
            numbers: true,
            symbols: true
        })
    }
}

module.exports = {
    LoginConstants
}