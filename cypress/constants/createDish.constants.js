const chance = require('chance').Chance();
const {StringGenerator} = require('../middlewares/generators')

class CreateDishConstants {
    constructor() {
        this.generator = new StringGenerator()
        this.validFormatValidSizeFilePath = 'cypress/fixtures/files/normalImage.png'
        this.secondValidFormatValidSizeFilePath = 'cypress/fixtures/files/normalIcon.png'
        this.validFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeImage.jpg'
        this.invalidFormatValidSizeFilePath = 'cypress/fixtures/files/normalFile.gif'
        this.invalidFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeFile.mp4'
        this.randomName = chance.name()
        this.updateRandomName = chance.name()
        this.randomShortName = this.generator.generateUpperString(3)  + '.' + this.generator.generateLowerString(3)
        this.updateRandomShortName = this.generator.generateUpperString(3)  + '.' + this.generator.generateLowerString(3)
        this.randomDescription = chance.string({length: 30})
        this.updateRandomDescription = chance.string({length: 30})
        this.randomNameLargestring = chance.string({length: 31})
        this.randomDescriptionLargeString = chance.string({length: 256})
        this.randomShortNameLowerString = this.generator.generateLowerString(3)  + '.' + this.generator.generateUsualString(3)
        this.randomShortNameMaxString = this.generator.generateUsualString(5)  + '.' + this.generator.generateUsualString(5)
        this.randomShortNameSecondNullString = this.generator.generateUpperString(5) +'.'

    }
}

module.exports = {
    CreateDishConstants
}