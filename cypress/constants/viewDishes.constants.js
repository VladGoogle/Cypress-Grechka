const chance = require('chance').Chance();

class ViewDishesConstants {
    constructor() {
        this.randomName = chance.name()
        this.randomDescription = chance.string({length: 30})
        this.randomNameLargeString = chance.string({length: 31})
        this.randomDescriptionLargeString = chance.string({length: 256})
    }
}

module.exports = {
    ViewDishesConstants
}