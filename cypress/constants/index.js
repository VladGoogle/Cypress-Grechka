const {ResetPasswordConstants}= require('./resetPassword.constants')
const {CreateDishConstants} = require('./createDish.constants')
const {CreateSubcategoryConstants} = require('./createSubcategory.constants')
const {CreateCategoryConstants} = require('./createCategory.constants')
const {CreateDrinkConstants} = require('./createDrink.constants')
const {ViewDishesConstants}= require('./viewDishes.constants')
const {ViewDrinksConstants}= require('./viewDrinks.constants')
const {LoginConstants} = require('./login.constants')
const {RegisterConstants} = require('./register.constants')

module.exports = {
    ResetPasswordConstants, CreateDishConstants, CreateSubcategoryConstants, CreateCategoryConstants, CreateDrinkConstants, ViewDishesConstants, ViewDrinksConstants, LoginConstants, RegisterConstants
}