const chance = require('chance').Chance();
const {StringGenerator} = require('../middlewares/generators')

class CreateSubcategoryConstants {
    constructor() {

        this.stringGenerator = new StringGenerator()
        this.validFormatValidSizeFilePath = 'cypress/fixtures/files/normalImage.png'
        this.secondValidFormatValidSizeFilePath = 'cypress/fixtures/files/normalIcon.png'
        this.validFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeImage.jpg'
        this.invalidFormatValidSizeFilePath = 'cypress/fixtures/files/normalFile.gif'
        this.invalidFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeFile.mp4'
        this.randomName = chance.name()
        this.randomUpdateName = this.stringGenerator.generateUsualString(30)
        this.randomNameLargeString = chance.string({length: 256})

    }
}

module.exports = {
    CreateSubcategoryConstants
}