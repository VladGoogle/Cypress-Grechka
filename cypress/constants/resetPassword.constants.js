const chance = require('chance').Chance();

class ResetPasswordConstants {
    constructor() {
        this.emailForLogin = 'vlad.google@portion.club'
        this.passwordForLogin = 'Asdrtyjklmnb1@'
        this.randomInvalidFormatEmail = '.' + chance.email({domain: 'portion.club'})
        this.randomValidFormatEmail = chance.email({domain: 'portion.club'})
    }
}

module.exports = {
    ResetPasswordConstants
}