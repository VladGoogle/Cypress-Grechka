const chance = require('chance').Chance();
const {StringGenerator} = require('../middlewares/generators')

class CreateDrinkConstants {

    constructor() {

        this.validFormatValidSizeFilePath = 'cypress/fixtures/files/normalImage.png'
        this.secondValidFormatValidSizeFilePath = 'cypress/fixtures/files/normalIcon.png'
        this.validFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeImage.jpg'
        this.invalidFormatValidSizeFilePath = 'cypress/fixtures/files/normalFile.gif'
        this.invalidFormatInvalidSizeFilePath = 'cypress/fixtures/files/largeFile.mp4'
        this.randomName = chance.name()
        this.updateRandomName = chance.name()
        this.randomDescription = chance.string({length: 30})
        this.updateRandomDescription = chance.string({length: 30})
        this.randomNameLargeString = chance.string({length: 31})
        this.randomDescriptionLargeString = chance.string({length: 256})

    }
}

module.exports = {
    CreateDrinkConstants
}