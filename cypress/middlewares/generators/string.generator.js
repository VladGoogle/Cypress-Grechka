

class StringGenerator {

    generateLowerString(length) {
        let stringLength = 8,
            charset = "abcdefghijklmnopqrstuvwxyz",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    generateUpperString(length) {
        let charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    generateAlphabetString(length) {
        let stringLength = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWX",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    generateNumberString(length) {
        let stringLength = 8,
            charset = "0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    generateSymbolString(length) {
        let stringLength = 8,
            charset = ".+*?^$()[]{}|_%#!~@,-=&\/:;<>",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }


    generateUsualString(length) {
        let stringLength = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }

    generateRandomFloatNumber(min, max, decimals){
        const str = (Math.random() * (max - min) + min).toFixed(decimals);

        return parseFloat(str);
    }

}

module.exports = {
    StringGenerator
}