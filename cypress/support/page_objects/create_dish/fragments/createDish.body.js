

class CreateDishPage {

    getContentWrapper() {
        return cy.get('.content-wrapper');
    }

    getCreateDishTitle() {
        return cy.contains('Create new dish');
    }

    getCardBody(){
        return cy.get('.card-body');
    }

    getNameInput(){
        return cy.get('[id="name"]');
    }

    getShortNameInput(){
        return cy.get('[id="shortName"]');
    }

    getDescriptionInput(){
        return cy.get('[id="description"]')
    }

    getPlateImageInput(){
        return cy.get('.custom-file-input').get('[name=plateImage]');
    }

    getSearchImageInput(){
        return cy.get('.custom-file-input').get('[name=searchImage]');
    }

    getFullImageInput(){
        return cy.get('.custom-file-input').get('[name=fullImage]');
    }

    getHalfImageInput(){
        return cy.get('.custom-file-input').get('[name=halfImage]');
    }

    getThirdImageInput(){
        return cy.get('.custom-file-input').get('[name=thirdImage]');
    }

    getQuarterImageInput(){
        return cy.get('.custom-file-input').get('[name=quarterImage]');
    }

    getProteinsInput() {
        return cy.get('[name="proteinsPer100Grams"]');
    }

    getFatsInput() {
        return cy.get('[name="fatsPer100Grams"]');
    }

    getCarbsInput() {
        return cy.get('[name="carbsPer100Grams"]');
    }

    getFibersInput() {
        return cy.get('[name="fibersPer100Grams"]');
    }

    getCaloriesInput() {
        return cy.get('[name="caloriesPer100Grams"]');
    }

    getSpoonGramsInput() {
        return cy.get('[name="gramsInSpoon"]');
    }

    getPalmGramsInput() {
        return cy.get('[name="gramsInPalm"]');
    }

    getCategorySelector(){
        return cy.get('[name="category"]');
    }

    getSubcategorySelector(){
        return cy.get('[name="subcategory"]');
    }

    getVitaminsSelector(){
        return cy.get('[name="vitamins"]');
    }

    getSubmitButton(){
        return cy.contains('Submit');
    }

    getViewDishLink(){
        return cy.get('.breadcrumb-item');
    }

}

module.exports = {
    CreateDishPage
}