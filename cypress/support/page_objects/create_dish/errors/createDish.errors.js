

class CreateDishErrors {

    getNameRequiredError() {
        return cy.contains('The name field is required.')
    }

    getDescriptionRequiredError() {
        return cy.contains('The description field is required.')
    }

    getPlateImageRequiredError() {
        return cy.contains('The plate image field is required.')
    }

    getSearchImageRequiredError() {
        return cy.contains('The search image field is required.')
    }

    getFullImageRequiredError() {
        return cy.contains('The full image field is required.')
    }

    getHalfImageRequiredError() {
        return cy.contains('The half image field is required.')
    }

    getThirdImageRequiredError() {
        return cy.contains('The third image field is required.')
    }

    getQuarterImageRequiredError() {
        return cy.contains('The quarter image field is required.')
    }

    getProteinsRequiredError() {
        return cy.contains('The proteins per 100 grams field is required.')
    }

    getFatsRequiredError() {
        return cy.contains('The fats per 100 grams field is required.')
    }

    getCarbsRequiredError() {
        return cy.contains('The carbs per 100 grams field is required.')
    }

    getFibersRequiredError() {
        return cy.contains('The fibers per 100 grams field is required.')
    }

    getCaloriesRequiredError() {
        return cy.contains('The calories per 100 grams field is required.')
    }

    getSpoonGramsRequiredError() {
        return cy.contains('The grams in spoon field is required.')
    }

    getPalmGramsRequiredError() {
        return cy.contains('The grams in palm field is required.')
    }

    getCategoryRequiredError() {
        return cy.contains('The category field is required.')
    }

    getSubcategoryRequiredError() {
        return cy.contains('The subcategory field is required.')
    }

    getVitaminsRequiredError() {
        return cy.contains('The vitamins field is required.')
    }

    getNameInvalidError() {
        return cy.contains('The name must not be greater than 30 characters.')

    }

    getShortNameInvalidError() {
        return cy.contains('Short name must start with a capital letter followed by a special character (.-) or a space and then the second word.')
    }

    getShortNameMaxError() {
        return cy.contains('The short name must not be greater than 10 characters.')
    }

    getDescriptionMaxError() {
        return cy.contains('The description must not be greater than 255 characters.')
    }

    getPlateImageInvalidFormatError() {
        return cy.contains('The plate image must be a file of type: jpg, png, jpeg.')
    }

    getSearchImageInvalidFormatError() {
        return cy.contains('The search image must be a file of type: jpg, png, jpeg.')
    }

    getFullImageInvalidFormatError() {
        return cy.contains('The full image must be a file of type: jpg, png, jpeg.')
    }

    getHalfImageInvalidFormatError() {
        return cy.contains('The half image must be a file of type: jpg, png, jpeg.')
    }

    getThirdImageInvalidFormatError() {
        return cy.contains('The third image must be a file of type: jpg, png, jpeg.')
    }

    getQuarterImageInvalidFormatError() {
        return cy.contains('The quarter image must be a file of type: jpg, png, jpeg.')
    }

    getPlateImageInvalidSizeError() {
        return cy.get('[id="error-plate-image"]').contains('File size is larger than 5 MB')
    }

    getSearchImageInvalidSizeError() {
        return cy.get('[id="error-search-image"]').contains('File size is larger than 5 MB')
    }

    getFullImageInvalidSizeError() {
        return cy.get('[id="error-full-image"]').contains('File size is larger than 5 MB')
    }

    getHalfImageInvalidSizeError() {
        return cy.get('[id="error-half-image"]').contains('File size is larger than 5 MB')
    }

    getThirdImageInvalidSizeError() {
        return cy.get('[id="error-third-image"]').contains('File size is larger than 5 MB')
    }

    getQuarterImageInvalidSizeError() {
        return cy.get('[id="error-quarter-image"]').contains('File size is larger than 5 MB')
    }

    getSuccessCreateDishMsg() {
        return cy.contains('Dish has been created successfully')
    }

    getSuccessUpdateDishMsg() {
        return cy.contains('Dish has been updated successfully')
    }

}

module.exports = {
    CreateDishErrors
}