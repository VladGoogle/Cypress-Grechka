

class CreateDrinkPage {

    getContentWrapper() {
        return cy.get('.content-wrapper');
    }

    getCreateDrinkTitle() {
        return cy.contains('Create new drink');
    }

    getCardBody(){
        return cy.get('.card-body');
    }

    getNameInput(){
        return cy.get('[id="name"]');
    }

    getDescriptionInput(){
        return cy.get('[id="description"]')
    }

    getCupImageInput(){
        return cy.get('.custom-file-input').get('[name=cupImage]');
    }

    getSearchImageInput(){
        return cy.get('.custom-file-input').get('[name=searchImage]');
    }

    getProteinsInput() {
        return cy.get('[name="proteinsPer100Grams"]');
    }

    getFatsInput() {
        return cy.get('[name="fatsPer100Grams"]');
    }

    getCarbsInput() {
        return cy.get('[name="carbsPer100Grams"]');
    }

    getFibersInput() {
        return cy.get('[name="fibersPer100Grams"]');
    }

    getCaloriesInput() {
        return cy.get('[name="caloriesPer100Grams"]');
    }

    getSubcategorySelector(){
        return cy.get('[name="subcategory"]');
    }

    getVitaminsSelector(){
        return cy.get('[name="vitamins"]');
    }

    getSubmitButton(){
        return cy.contains('Submit');
    }

    getViewDishLink(){
        return cy.get('.breadcrumb-item');
    }

}

module.exports = {
    CreateDrinkPage
}