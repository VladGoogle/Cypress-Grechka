

class CreateDrinkErrors {

    getNameRequiredError() {
        return cy.contains('The name field is required.')
    }

    getDescriptionRequiredError() {
        return cy.contains('The description field is required.')
    }

    getCupImageRequiredError() {
        return cy.contains('The cup image field is required.')
    }

    getSearchImageRequiredError() {
        return cy.contains('The search image field is required.')
    }

    getProteinsRequiredError() {
        return cy.contains('The proteins per 100 grams field is required.')
    }

    getFatsRequiredError() {
        return cy.contains('The fats per 100 grams field is required.')
    }

    getCarbsRequiredError() {
        return cy.contains('The carbs per 100 grams field is required.')
    }

    getFibersRequiredError() {
        return cy.contains('The fibers per 100 grams field is required.')
    }

    getCaloriesRequiredError() {
        return cy.contains('The calories per 100 grams field is required.')
    }

    getSubcategoryRequiredError() {
        return cy.contains('The subcategory field is required.')
    }

    getVitaminsRequiredError() {
        return cy.contains('The vitamins field is required.')
    }

    getNameInvalidError() {
        return cy.contains('The name must not be greater than 30 characters.')

    }

    getDescriptionMaxError() {
        return cy.contains('The description must not be greater than 255 characters.')
    }

    getCupImageInvalidFormatError() {
        return cy.contains('The cup image must be a file of type: jpg, png, jpeg.')
    }

    getSearchImageInvalidFormatError() {
        return cy.contains('The search image must be a file of type: jpg, png, jpeg.')
    }

    getCupImageInvalidSizeError() {
        return cy.contains('File size is larger than 5 MB')
    }

    getSearchImageInvalidSizeError() {
        return cy.contains('File size is larger than 5 MB')
    }

    getSuccessCreateDrinkMsg() {
        return cy.contains('Drink has been created successfully')
    }

    getSuccessUpdateDrinkMsg() {
        return cy.contains('Drink has been updated successfully')
    }

}

module.exports = {
    CreateDrinkErrors
}