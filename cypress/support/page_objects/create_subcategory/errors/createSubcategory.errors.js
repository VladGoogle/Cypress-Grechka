

class CreateSubcategoryErrors {

    getNameRequiredError() {
        return cy.contains('The name field is required.')
    }

    getNameMaxError() {
        return cy.contains('The name must not be greater than 255 characters.')
    }

    getIconRequiredError() {
        return cy.contains('The icon field is required.')
    }

    getIconInvalidFormatError() {
        return cy.contains('The icon must be a file of type: jpg, png, jpeg.')
    }

    getIconInvalidSizeError() {
        return cy.contains('File size is larger than 5 MB')
    }

    getDuplicateSubcategoryError() {
        return cy.contains('This subcategory has already created')
    }

    getSuccessCreateSubcategoryMsg() {
        return cy.contains('Subcategory has been created successfully')
    }

    getSuccessUpdateSubcategoryMsg() {
        return cy.contains('Subcategory has been updated successfully')
    }


}

module.exports = {
    CreateSubcategoryErrors
}