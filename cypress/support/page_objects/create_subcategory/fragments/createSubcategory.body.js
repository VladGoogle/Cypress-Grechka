

class CreateSubcategoryPage {

    getCreateSubcategoryCard(){
        return cy.get('.content-wrapper');
    }

    getCreateSubcategoryTitle() {
        return cy.contains('Create subcategory');
    }

    getNameInput(){
        return cy.get('[id="name"]');
    }

    getUpdateNameInput() {
        return cy.get('#title')
    }

    getIconInput(){
        return cy.get('.custom-file-input')
    }

    getCategorySelector(){
        return cy.get('[name="category"]');
    }

    getSubmitButton(){
        return cy.contains('Submit');
    }

    getViewDishLink(){
        return cy.get('.breadcrumb-item');
    }

}

module.exports = {
    CreateSubcategoryPage
}