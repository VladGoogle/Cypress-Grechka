

class ViewDishesErrors {

    getNameMaxError() {
        return cy.contains('The name must not be greater than 30 characters.')
    }

    getDescriptionMaxError() {
        return cy.contains('The description must not be greater than 255 characters.')
    }

    getSpoonGramsMinError() {
        return cy.contains('The grams in spoon must be at least 1.')
    }

    getPalmGramsMinError() {
        return cy.contains('The grams in palm must be at least 1.')
    }

    getNoRecordsFoundError() {
        return cy.contains('No records found')
    }

}

module.exports = {
    ViewDishesErrors
}