

class ViewDishesPage {

    getContentWrapper() {
        return cy.get('.content-wrapper');
    }

    getViewDishesTitle() {
        return cy.contains('Dishes');
    }

    getSearchBlock() {
        return cy.get('div[id="search"]')
    }

    getNameInput(){
        return cy.get('[name="name"]');
    }

    getDescriptionInput(){
        return cy.get('[name="description"]')
    }

    getOrderBySelector(){
        return cy.get('[name="orderBy"]');
    }

    getOrderSelector(){
        return cy.get('[name="order"]');
    }

    getSpoonGramsInput() {
        return cy.get('[name="gramsInSpoon"]');
    }

    getSpoonGramsOption() {
        return cy.get('[name="gramsInSpoonSign"]');
    }

    getPalmGramsInput() {
        return cy.get('[name="gramsInPalm"]');
    }

    getPalmGramsOption() {
        return cy.get('[name="gramsInPalmSign"]');
    }

    getCategorySelector(){
        return cy.get('[name="category"]');
    }

    getSubcategorySelector(){
        return cy.get('[name="subcategory"]');
    }

    getItemsPerPageSelector(){
        return cy.get('[name="itemsPerPage"]');
    }

    getSearchButton(){
        return cy.contains('Search');
    }

    getTableBlock(){
        return cy.get('table[class="table table-bordered"]');
    }

    getCategoryColumn() {
        return cy.get('table > tbody > tr > td:nth-child(5)');
    }

    getPaginationBlock(){
        return cy.get('.pagination');
    }

}

module.exports = {
    ViewDishesPage
}