

class ResetPasswordErrors {

    getInvalidLoginError() {
        return cy.get('.login-box').contains('The email must be a valid email address.');
    }
}

module.exports = {
    ResetPasswordErrors
}