

class ResetPasswordPage {

    getLoginBox() {
        return cy.get('.login-box');
    }
    getLoginLogo(){
        return cy.get('.login-logo');
    }
    getLoginBoxMessage(){
        return cy.get('.login-box-msg');
    }
    getEmail(){
        return cy.get('.login-box').get('[name="email"]');
    }
    getSubmitButton(){
        return cy.get('.login-box').contains('Request new password');
    }
    getRegisterLink() {
        return cy.get('.login-box').contains('Register a new membership');
    }
    getLoginLink() {
        return cy.get('.login-box').contains('Login');
    }
}

module.exports = {
    ResetPasswordPage
}