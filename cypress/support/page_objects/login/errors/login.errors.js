

class LoginErrors {

    getEmailRequiredError() {
        return cy.contains('The email field is required.')
    }

    getPasswordRequiredError() {
        return cy.contains('The password field is required.')
    }

    getInvalidCredError() {
        return cy.contains('Invalid credentials')
    }

    getIncorrectEmailError() {
        return cy.contains('Your E-mail is incorrect, please use given email "example@portion.club"')
    }

    getInvalidEmailError() {
        return cy.contains('The email must be a valid email address.')
    }

}

module.exports = {
    LoginErrors
}