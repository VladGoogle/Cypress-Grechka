

class LoginPage {

    getLoginBox() {
        return cy.get('.login-box');
    }

    getLoginLogo() {
        return cy.contains('Proper Eating')
    }

    getEmailInput(){
        return cy.get('input[name="email"]');
    }

    getPasswordInput(){
        return cy.get('input[name="password"]');
    }

    getRememberMeLabel() {
        return cy.get('label[for="remember"]');
    }

    getSignInButton(){
        return cy.contains('Sign In');
    }

    getForgotPasswordLink() {
        return cy.contains('Forgot password');
    }

    getRegisterLink() {
        return cy.contains('Create new account');
    }
}

module.exports = {
    LoginPage
}