

class CreateCategoryPage {

    getCreateCategoryCard(){
        return cy.get('.content-wrapper');
    }

    getCreateCategoryTitle() {
        return cy.contains('Create Category');
    }

    getNameInput(){
        return cy.get('[id="title"]');
    }

    getSubmitButton(){
        return cy.contains('Submit');
    }

    getViewDishLink(){
        return cy.get('.breadcrumb-item');
    }

}

module.exports = {
    CreateCategoryPage
}