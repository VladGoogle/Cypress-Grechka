

class CreateCategoryErrors {

    getNameRequiredError() {
        return cy.contains('The name field is required.')
    }

    getNameMaxError() {
        return cy.contains('The name must not be greater than 30 characters.')
    }

    getDuplicateCategoryError() {
        return cy.contains('This category has already created')
    }

    getSuccessCreateCategoryMsg() {
        return cy.contains('Category has been created successfully')
    }

    getSuccessUpdateCategoryMsg() {
        return cy.contains('Category has been updated successfully')
    }

}

module.exports = {
    CreateCategoryErrors
}