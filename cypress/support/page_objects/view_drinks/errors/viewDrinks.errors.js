

class ViewDrinksErrors {

    getNameMaxError() {
        return cy.contains('The name must not be greater than 30 characters.')
    }

    getDescriptionMaxError() {
        return cy.contains('The description must not be greater than 255 characters.')
    }

    getNoRecordsFoundError() {
        return cy.contains('No records found')
    }

}

module.exports = {
    ViewDrinksErrors
}