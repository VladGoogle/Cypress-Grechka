

class ViewDrinksPage {


    getContentWrapper() {
        return cy.get('.content-wrapper');
    }

    getBody() {
        return cy.get('div[class="content-wrapper"]');
    }

    getViewDrinksTitle() {
        return cy.contains('Drinks');
    }

    getSearchBlock() {
        return cy.get('div[id="search"]')
    }

    getNameInput(){
        return cy.get('[name="name"]');
    }

    getDescriptionInput(){
        return cy.get('[name="description"]')
    }

    getOrderBySelector(){
        return cy.get('[name="orderBy"]');
    }

    getOrderSelector(){
        return cy.get('[name="order"]');
    }

    getSubcategorySelector(){
        return cy.get('[name="subcategory"]');
    }

    getItemsPerPageSelector(){
        return cy.get('[name="itemsPerPage"]');
    }

    getSearchButton(){
        return cy.contains('Search');
    }

    getTableBlock(){
        return cy.get('table[class="table table-bordered"]');
    }

    getPaginationBlock(){
        return cy.get('.pagination');
    }



}

module.exports = {
    ViewDrinksPage
}