const xpath = require('cypress-xpath')

class SidebarFragment {

    getMainSidebar() {
        return cy.get('.main-sidebar');
    }

    getBrandLink(){
        return cy.get('.brand-link');
    }

    getUserPanel(){
        return cy.get('.user-panel');
    }

    getDishesDropdown(){
        return cy.contains('Dishes');
    }

    getCreateDishTab(){
        return cy.contains('Create dish');
    }

    getViewDishesTab(){
        return cy.contains('View dishes');
    }

    getCategoriesDropdown() {
        return cy.xpath('/html/body/div/aside/div/nav/ul/li[2]/a')
    }

    getViewCategoriesTab() {
        return cy.contains('View categories')
    }

    getCreateSubcategoryTab() {
        return cy.contains('Create subcategory');
    }

    getCreateCategoryTab() {
        return cy.contains('Create category');
    }

    getDrinkCategoriesDropdown() {
        return cy.xpath('/html/body/div/aside/div/nav/ul/li[3]/a')
    }

    getCreateDrinkTab() {
        return cy.contains('Create Drink');
    }

    getViewDrinkTab() {
        return cy.contains('View Drink');
    }

    getLogoutButton(){
        return cy.contains('Logout');
    }

}

module.exports = {
    SidebarFragment
}