

class RegisterPage {

    getRegisterBox() {
        return cy.get('.register-box');
    }

    getFullNameInput(){
        return cy.get('input[name="name"]');
    }

    getEmailInput(){
        return cy.get('input[name="email"]');
    }

    getPasswordInput(){
        return cy.get('input[name="password"]');
    }

    getPasswordConfirmInput(){
        return cy.get('input[name="password_confirmation"]');
    }

    getRegisterButton(){
        return cy.get('button').contains('Register');
    }

    getLoginLink() {
        return cy.contains('I already have an account');
    }
}

module.exports = {
    RegisterPage
}