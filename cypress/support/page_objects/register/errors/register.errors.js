

class RegisterErrors {

    getNameRequiredError() {
        return cy.contains('The name field is required.')
    }

    getEmailRequiredError() {
        return cy.contains('The email field is required.')
    }

    getPasswordRequiredError() {
        return cy.contains('The password field is required.')
    }

    getNameMinError() {
        return cy.contains('The name must be at least 2 characters.')
    }

    getNameMaxError() {
        return cy.contains('The name must not be greater than 32 characters.')
    }

    getEmailInvalidDomainError() {
        return cy.contains('Your E-mail is incorrect, please use given email "example@portion.club"')
    }

    getEmailInvalidFormatError() {
        return cy.contains('The email must be a valid email address.')
    }

    getEmailMaxError() {
        return cy.contains('The email must not be greater than 32 characters.')
    }

    getPasswordMinError() {
        return cy.contains('The password must be at least 8 characters.')
    }

    getPasswordMaxError() {
        return cy.contains('The password must not be greater than 16 characters.')
    }

    getPasswordLettersError() {
        return cy.contains('The password must contain at least one uppercase and one lowercase letter.')
    }

    getPasswordNumbersError() {
        return cy.contains('The password must contain at least one number.')
    }

    getPasswordConfirmError() {
        return cy.contains('The password confirmation does not match.')
    }

    getExistedEmailError() {
        return cy.contains('This e-mail already exists, try another one')
    }

    getSuccessRegisterMsg() {
        return cy.contains('You were successfully registered, enter your credentials here')
    }

}

module.exports = {
    RegisterErrors
}