require('cypress-file-upload')
const {StringGenerator} = require('../middlewares/generators')

const stringGenerator = new StringGenerator()

 Cypress.Commands.add('login', (email, password) => {
     cy.visit('/login')
     cy.get('[name="email"]').type(email)
     cy.get('[name="password"]').type(password)
     cy.contains('Sign In').click()
 })

Cypress.Commands.add('typeEmailAndPassword', (email, password) => {
    cy.get('[name="email"]').type(email)
    cy.get('[name="password"]').type(password)
})


Cypress.Commands.add('uploadDishImage', (filePath) => {
    cy.get('.custom-file-input').get('[name=plateImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=searchImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=fullImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=halfImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=thirdImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=quarterImage]').selectFile(filePath)
})

Cypress.Commands.add('uploadDrinkImage', (filePath) => {
    cy.get('.custom-file-input').get('[name=cupImage]').selectFile(filePath)
    cy.get('.custom-file-input').get('[name=searchImage]').selectFile(filePath)
})

Cypress.Commands.add('uploadSubcategoryImage', (filePath) => {
    cy.get('.custom-file-input').selectFile(filePath)
})



Cypress.Commands.add('validateMinBrowserErrorMsg', (inputSelector, buttonSelector) => {
    cy.get(`[name=${inputSelector}]`).type('-1')
    cy.contains(buttonSelector).click().wait(1000)
    cy.get('input:invalid').should('have.length', 1)
    cy.get(`[name=${inputSelector}]`).clear()
})

Cypress.Commands.add('validateFloatBrowserErrorMsg', (inputSelector, buttonSelector) => {
    cy.get(`[name=${inputSelector}]`).clear().type(`${stringGenerator.generateRandomFloatNumber(5, 20,3)}`)
    cy.contains(buttonSelector).click().wait(1000)
    cy.get('input:invalid').should('have.length', 1)
    cy.get(`[name=${inputSelector}]`).clear()
})

Cypress.Commands.add('enterNutrients', (inputSelector) => {
    cy.get(`[name=${inputSelector}]`).clear().type(`${stringGenerator.generateRandomFloatNumber(5, 20,2)}`)
})

Cypress.Commands.add('checkPagination', (itemsOption) => {

    //Change option of "Items per page" selector to 10
    cy.get('[name="itemsPerPage"]').select(`${itemsOption}`)
    cy.contains('Search').click()

    //Table should display x items per page
    cy.get('table > tbody > tr').should('have.length', itemsOption)
})

Cypress.Commands.add('getEditButton', (name) => {
    cy.get(`[id="edit-${name}"]`).click()
})

Cypress.Commands.add('getViewCategoriesEditButton', (name) => {
    cy.get(`a[id="edit-${name}"]`).click()
})


Cypress.Commands.add('getDeleteButton', (name) => {
    cy.get(`button[id="delete-${name}"]`).click()
})




