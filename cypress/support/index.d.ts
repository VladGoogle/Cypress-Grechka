declare namespace Cypress {
    interface Chainable<Subject = any> {
        login(email: string, password: string): Cypress.Chainable<null>;
        getEditButton(name: string): Cypress.Chainable<null>;
        getViewCategoriesEditButton(name: string): Cypress.Chainable<null>;
        getDeleteButton(name: string): Cypress.Chainable<null>;
        typeEmailAndPassword(email: string, password: string): Cypress.Chainable<null>;
        uploadDishImage(filePath: string): Cypress.Chainable<null>;
        uploadDrinkImage(filePath: string): Cypress.Chainable<null>;
        uploadSubcategoryImage(filePath: string): Cypress.Chainable<null>;
        enterNutrients(inputSelector: any): Cypress.Chainable<null>;
        checkPagination(itemsOption: number): Cypress.Chainable<null>;
        validateMinBrowserErrorMsg(inputSelector: any, buttonSelector: any): Cypress.Chainable<null>;
        validateFloatBrowserErrorMsg(inputSelector: any, buttonSelector: any): Cypress.Chainable<null>;

    }
}