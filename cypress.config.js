const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: '7ycyba',
  e2e: {
    viewportWidth: 1280,
    viewportHeight: 700,
    baseUrl: "",
    //specPattern: "cypress/integration/api-tests",
    videoCompression: false,
    screenshotsFolder: "cypress/screenshots",
    trashAssetsBeforeRuns: true,
    chromeWebSecurity: false,
    defaultCommandTimeout: 5000
  },

});
