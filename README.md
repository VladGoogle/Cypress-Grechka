<h1 align="center">Welcome to e2e-tests-adminpanel 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> E2E tests for admin panel

## Install

```sh
npm install
```

## Usage

```sh
Open Cypress GUI - npm run cypress:open
```

## Run tests

```sh
npm run cypress:open
```

## Author

👤 **Vladislav**

* Github: [@VladGoogle](https://github.com/VladGoogle)

